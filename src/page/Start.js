import React, { Component } from 'react';
import Navbars from './Navbar';
import Swal from 'sweetalert2';

class Start extends Component {
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			Swal.fire('คำเตือน!', 'กรุณา Login ก่อน', 'warning');
		}
	}

	render() {
		const { children } = this.props;
		return (
			<div style={{ width: '100vw', height: '100vh' }}>
				<Navbars />
				{children}
			</div>
		);
	}
}
export default Start;
