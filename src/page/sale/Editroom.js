import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label, FormText } from 'reactstrap';
import { post, get, sever } from '../../service/service';
import Swal from 'sweetalert2';
let api = sever + '/image/room/';

export default class Editroom extends Component {
	constructor(props) {
		super(props);
		let {
			amount_meeting,
			chair_meeting,
			detail_meeting,
			name_meeting,
			type_meeting,
			tool_meeting,
			location_meeting,
			deposit,
			id_meeting
		} = this.props.location.state;
		this.state = {
			type_meeting,
			amount_meeting,
			chair_meeting,
			detail_meeting,
			name_meeting,
			type_meeting,
			tool_meeting,
			location_meeting,
			deposit,
			id_meeting
		};
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	onChangs = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	Submit = async () => {
		let {
			amount_meeting,
			chair_meeting,
			detail_meeting,
			filebase64,
			name_meeting,
			type_meeting,
			tool_meeting,
			location_meeting,
			deposit,
			id_meeting
		} = this.state;
		if (
			!amount_meeting ||
			!chair_meeting ||
			!detail_meeting ||
			!name_meeting ||
			!type_meeting ||
			!tool_meeting ||
			!location_meeting ||
			!deposit
		) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			let obj = {
				amount_meeting: Number(amount_meeting),
				chair_meeting: Number(chair_meeting),
				detail_meeting,
				image: filebase64 ? filebase64.split(',')[1] : '',
				name_meeting,
				type_meeting,
				tool_meeting,
				location_meeting,
				deposit,
				id_meeting
			};
			try {
				let res = await post('/update_room', obj);
				if (res.success) {
					Swal.fire('สำเร็จ', 'แก้ไขสำเร็จ', 'success').then(() => {
						this.props.history.push('/SaleHomeTwo');
						window.location.reload();
					});
				} else {
					Swal.fire('ผิดพลาด', 'แก้ไขไม่สำเร็จ', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', 'แก้ไขไม่สำเร็จ', 'error');
			}
		}
	};
	render() {
		let {
			amount_meeting,
			chair_meeting,
			detail_meeting,
			filebase64,
			name_meeting,
			type_meeting,
			tool_meeting,
			location_meeting,
			deposit,
			id_meeting
		} = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>ลงทะเบียนห้องประชุม</div>
								</div>
								<Form style={{ paddingTop: '30px' }}>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ชื่อห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												value={name_meeting}
												type="text"
												name="name_meeting"
												placeholder="ชื่อห้องประชุม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนเก้าอี้
										</Label>
										<Col sm={10}>
											<Input
												value={chair_meeting}
												type="number"
												name="chair_meeting"
												placeholder="จำนวนเก้าอี้"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleSelect" sm={2}>
											ขนาดห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												value={type_meeting}
												type="select"
												name="type_meeting"
												onChange={this.onChangs}
											>
												<option value="ห้องประชุมขนาดใหญ่">ห้องประชุมขนาดใหญ่</option>
												<option value="ห้องประชุมขนาดกลาง">ห้องประชุมขนาดกลาง</option>
												<option value="ห้องประชุมขนาดเล็ก">ห้องประชุมขนาดเล็ก</option>
											</Input>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											อุปกรณ์เสริมในห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												value={tool_meeting}
												type="text"
												name="tool_meeting"
												placeholder="อุปกรณ์เสริม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											สถานที่/ที่ตั้ง
										</Label>
										<Col sm={10}>
											<Input
												value={location_meeting}
												type="text"
												name="location_meeting"
												placeholder="ที่ตั้ง สถานที่ ที่อยู่ ห้อง"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ราคาลงทะเบียน
										</Label>
										<Col sm={10}>
											<Input
												value={amount_meeting}
												type="number"
												name="amount_meeting"
												placeholder="ราคาลงทะเบียน"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ราคามัดจำ
										</Label>
										<Col sm={10}>
											<Input
												value={deposit}
												type="number"
												name="deposit"
												placeholder="ราคามัดจำ"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleText" sm={2}>
											รายระเอียดห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												type="textarea"
												value={detail_meeting}
												name="detail_meeting"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleFile" sm={2}>
											ภาพห้องประชุม
										</Label>
										<Col sm={10}>
											<div style={{ display: 'flex' }}>
												{!filebase64 ? (
													<img src={api + id_meeting + '.png'} width={150} height={150} />
												) : (
													<img src={filebase64} width={150} height={150} />
												)}
												<div>
													<Input
														type="file"
														onChange={this.uploadImg}
														accept="image/x-png,image/gif,image/jpeg"
													/>
													<FormText color="muted">
														*ภาพห้องประชุมที่ต้องการนำมาลงทะเบียน*
													</FormText>
												</div>
											</div>
										</Col>
									</FormGroup>
									<FormGroup check row>
										<Col sm={12}>
											<div style={{ display: 'flex', justifyContent: 'center' }}>
												<Button
													color="primary"
													onClick={this.Submit}
													style={{ marginRight: '3rem' }}
												>
													ตกลง
												</Button>
												<Button
													color="danger"
													onClick={() => this.props.history.push('/SaleHomeTwo')}
												>
													ยกเลิก
												</Button>
											</div>
										</Col>
									</FormGroup>
								</Form>
								<div />
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
