import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Sector } from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import { Container, Row, Col, Input, Button, Table } from 'reactstrap';
import { get, sever } from '../../service/service';
import moment from 'moment';

let api = sever + '/image/room/';

const colors = scaleOrdinal(schemeCategory10).range();
const getPath = (x, y, width, height) => `M${x},${y + height}
            C${x + width / 3},${y + height} ${x + width / 2},${y + height / 3} ${x + width / 2}, ${y}
            C${x + width / 2},${y + height / 3} ${x + 2 * width / 3},${y + height} ${x + width}, ${y + height}
            Z`;
const TriangleBar = (props) => {
	const { fill, x, y, width, height } = props;
	return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
};
TriangleBar.propTypes = {
	fill: PropTypes.string,
	x: PropTypes.number,
	y: PropTypes.number,
	width: PropTypes.number,
	height: PropTypes.number
};
//---------------------------------------------------------------------------------------------------------------
const COLORS = [ '#1e77b4', '#ff7f0f', '#2aa02b', '#d62628', '#9467bd' ];
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
	const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
	const x = cx + radius * Math.cos(-midAngle * RADIAN);
	const y = cy + radius * Math.sin(-midAngle * RADIAN);
	return (
		<text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
			{`${(percent * 100).toFixed(0)}%`}
		</text>
	);
};

export default class Rate extends Component {
	constructor(props) {
		super(props);

		this.state = {
			evaluation: [],
			data: [],
			datas: []
		};
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/evaluation');
			let data = [
				{
					name: 'ควรปรับปรุง',
					female: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ควรปรับปรุง').length
				},
				{
					name: 'พอใช้',
					female: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'พอใช้').length
				},
				{
					name: 'ปานกลาง',
					female: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ปานกลาง').length
				},
				{
					name: 'ดี',
					female: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ดี').length
				},
				{
					name: 'ดีมาก',
					female: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ดีมาก').length
				}
			];
			let datas = [
				{
					name: 'ควรปรับปรุง',
					value: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ควรปรับปรุง').length
				},
				{
					name: 'พอใช้',
					value: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'พอใช้').length
				},
				{
					name: 'ปานกลาง',
					value: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ปานกลาง').length
				},
				{
					name: 'ดี',
					value: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ดี').length
				},
				{
					name: 'ดีมาก',
					value: res.result
						.filter((e) => Number(e.id_admin) === Number(userData.id))
						.filter((e) => e.level === 'ดีมาก').length
				}
			];
			this.setState({
				evaluation: res.result.filter((e) => Number(e.id_admin) === Number(userData.id)),
				data: data,
				datas: datas
			});
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		let { evaluation, data, datas } = this.state;
		return (
			<Container style={{ paddingTop: '1rem' }}>
				<Row>
					<Col sm={12}>
						<div id="cssheader">
							<div style={{ paddingRight: '10px', color: '#fff' }}>ผลการประเมินความพึงพอใจ</div>
						</div>
					</Col>
					<Col sm={6}>
						<div style={{ display: 'flex', justifyContent: 'center', marginTop: '4rem' }}>
							<BarChart
								width={500}
								height={300}
								data={data}
								margin={{
									top: 20,
									right: 30,
									left: 20,
									bottom: 5
								}}
							>
								<CartesianGrid strokeDasharray="3 3" />
								<XAxis dataKey="name" />
								<YAxis />
								<Bar
									dataKey="female"
									fill="#8884d8"
									shape={<TriangleBar />}
									label={{ position: 'top' }}
								>
									{data.map((entry, index) => (
										<Cell key={`cell-${index}`} fill={colors[index % 20]} />
									))}
								</Bar>
							</BarChart>
						</div>
					</Col>
					<Col sm={6}>
						<div style={{ display: 'flex', justifyContent: 'center' }}>
							<PieChart width={400} height={400}>
								<Pie
									data={datas}
									cx={200}
									cy={200}
									labelLine={false}
									label={renderCustomizedLabel}
									outerRadius={80}
									fill="#8884d8"
									dataKey="value"
								>
									{datas.map((entry, index) => (
										<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
									))}
								</Pie>
							</PieChart>
						</div>
					</Col>
					<Col xs={12}>
						<Table striped>
							<thead>
								<tr>
									<th>รูปภาพ</th>
									<th>ชื่อห้อง</th>
									<th>ประเภทห้อง</th>
									<th>ข้อเสนอแนะ</th>
									<th>ผลประเมิน</th>
								</tr>
							</thead>
							<tbody>
								{evaluation.length > 0 ? (
									evaluation.map((e) => (
										<tr>
											<td>
												<img src={api + e.id_meeting + '.png'} width={50} height={50} />
											</td>
											<td>{e.name_meeting}</td>
											<td>{e.type_meeting}</td>
											<td>{e.report}</td>
											<td
												style={{
													color:
														e.level === 'ควรปรับปรุง'
															? '#1e77b4'
															: e.level === 'พอใช้'
																? '#ff7f0f'
																: e.level === 'ปานกลาง'
																	? '#2aa02b'
																	: e.level === 'ดี' ? '#d62628' : '#9467bd'
												}}
											>
												{e.level}
											</td>
										</tr>
									))
								) : (
									<div>---- ไม่มีผลประเมิน ----</div>
								)}
							</tbody>
						</Table>
					</Col>
				</Row>
			</Container>
		);
	}
}
