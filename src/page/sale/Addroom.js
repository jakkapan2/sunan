import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label, FormText } from 'reactstrap';
import { post, get } from '../../service/service';
import Swal from 'sweetalert2';

export default class Addroom extends Component {
	constructor(props) {
		super(props);

		this.state = {
			type_meeting: 'ห้องประชุมขนาดใหญ่'
		};
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		try {
			let res = await get('/get_servicerate');
			this.setState({
				price_servicerate: res.result[0].price_servicerate,
				id_servicerate: res.result[0].id_servicerate,
				timestram_servicerate: res.result[0].timestram_servicerate
			});
		} catch (error) {
			console.log(error);
		}
	}

	onChangs = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	Submit = async () => {
		let {
			amount_meeting,
			chair_meeting,
			detail_meeting,
			filebase64,
			name_meeting,
			type_meeting,
			tool_meeting,
			location_meeting,
			deposit,
			price_servicerate
		} = this.state;
		if (
			!amount_meeting ||
			!chair_meeting ||
			!detail_meeting ||
			!filebase64 ||
			!name_meeting ||
			!type_meeting ||
			!tool_meeting ||
			!location_meeting ||
			!deposit
		) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			let userData = JSON.parse(localStorage.getItem('data'));
			let obj = {
				amount_meeting: Number(amount_meeting),
				chair_meeting: Number(chair_meeting),
				detail_meeting,
				image: filebase64.split(',')[1],
				name_meeting,
				type_meeting,
				tool_meeting,
				location_meeting,
				deposit,
				id_admin: Number(userData.id),
				servicerate: price_servicerate
			};
			try {
				let res = await post('/insert_room', obj);
				if (res.success) {
					Swal.fire('สำเร็จ', 'เพิ่มสำเร็จ', 'success').then(() => this.props.history.push('/SaleHomeTwo'));
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', 'เพิ่มไม่สำเร็จ', 'error');
			}
		}
	};
	render() {
		let { type_meeting, price_servicerate } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>ลงทะเบียนห้องประชุม</div>
								</div>
								<strong>
									*อัตราค่าบริการ {price_servicerate && price_servicerate.toLocaleString()}{' '}
									บาท/ลงทะเบียนห้องประชุม*
								</strong>
								<Form style={{ paddingTop: '30px' }}>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ชื่อห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												name="name_meeting"
												placeholder="ชื่อห้องประชุม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนเก้าอี้
										</Label>
										<Col sm={10}>
											<Input
												type="number"
												name="chair_meeting"
												placeholder="จำนวนเก้าอี้"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleSelect" sm={2}>
											ขนาดห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												value={type_meeting}
												type="select"
												name="type_meeting"
												onChange={this.onChangs}
											>
												<option value="ห้องประชุมขนาดใหญ่">ห้องประชุมขนาดใหญ่</option>
												<option value="ห้องประชุมขนาดกลาง">ห้องประชุมขนาดกลาง</option>
												<option value="ห้องประชุมขนาดเล็ก">ห้องประชุมขนาดเล็ก</option>
											</Input>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											อุปกรณ์เสริมในห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												name="tool_meeting"
												placeholder="อุปกรณ์เสริม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											สถานที่/ที่ตั้ง
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												name="location_meeting"
												placeholder="ที่ตั้ง สถานที่ ที่อยู่ ห้อง"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ราคาลงทะเบียน
										</Label>
										<Col sm={10}>
											<Input
												type="number"
												name="amount_meeting"
												placeholder="ราคาลงทะเบียน"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleEmail" sm={2}>
											ราคามัดจำ
										</Label>
										<Col sm={10}>
											<Input
												type="number"
												name="deposit"
												placeholder="ราคามัดจำ"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleText" sm={2}>
											รายระเอียดห้องประชุม
										</Label>
										<Col sm={10}>
											<Input type="textarea" name="detail_meeting" onChange={this.onChangs} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="exampleFile" sm={2}>
											ภาพห้องประชุม
										</Label>
										<Col sm={10}>
											<Input
												type="file"
												onChange={this.uploadImg}
												accept="image/x-png,image/gif,image/jpeg"
											/>
											<FormText color="muted">*ภาพห้องประชุมที่ต้องการนำมาลงทะเบียน*</FormText>
										</Col>
									</FormGroup>

									<FormGroup check row>
										<Col sm={12}>
											<div style={{ display: 'flex', justifyContent: 'center' }}>
												<Button
													color="primary"
													onClick={this.Submit}
													style={{ marginRight: '3rem' }}
												>
													ตกลง
												</Button>
												<Button color="danger">ยกเลิก</Button>
											</div>
										</Col>
									</FormGroup>
								</Form>
								<div />
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
