import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';

let api = sever + '/image/regis/';

export default class SaleCheckRoomavailability extends Component {
	constructor(props) {
		super(props);

		this.state = {
			registration: [],
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let registration = await get('/registration');
			this.setState({
				registration: registration.result
					.filter((e) => Number(e.id_admin) === Number(userData.id))
					.map((e) => ({
						name_meeting: e.name_meeting,
						timestam: moment(e.timestram_regis).add(543, 'y').format('LLLL'),
						type_payment: e.type_regis,
						price_payment: e.price_regis,
						id_payment: e.id_registration
					}))
			});
		} catch (error) {
			console.log(error);
		}
	}
	searchText = async (e) => {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let payments = await get('/registration');
			let texts = e.toLowerCase();
			let a = payments.result
				.filter((e) => Number(e.id_admin) === Number(userData.id))
				.sort((a, b) => moment(b.timestram_regis) - moment(a.timestam))
				.map((e) => ({
					name_meeting: e.name_meeting,
					timestam: moment(e.timestram_regis).add(543, 'y').format('LLLL'),
					type_payment: e.type_regis,
					price_payment: e.price_regis,
					id_payment: e.id_registration
				}))
				.filter(
					(el) =>
						String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.timestam).toLowerCase().indexOf(texts) > -1 ||
						String(el.type_payment).toLowerCase().indexOf(texts) > -1 ||
						String(el.price_payment).toLowerCase().indexOf(texts) > -1
				);
			this.setState({ registration: a });
		} catch (error) {
			console.log(error);
		}
	};
	render() {
		let { registration, modal, id_payment } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>ประวัติการชำระเงิน</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>

								<div>
									<Table striped>
										<thead>
											<tr>
												<th>ชื่อห้องประชุม</th>
												<th>การชำระ</th>
												<th>เป็นเงิน</th>
												<th>วันเวลาที่ชำระ</th>
												<th>ภาพประกอบ</th>
											</tr>
										</thead>
										<tbody>
											{registration.length > 0 ? (
												registration.map((e) => (
													<tr>
														<td>{e.name_meeting}</td>
														<td>{e.type_payment}</td>
														<td>{e.price_payment.toLocaleString()} บาท</td>
														<td>{e.timestam}</td>
														<td>
															<Button
																onClick={() => {
																	this.toggle();
																	this.setState({ id_payment: e.id_payment });
																}}
															>
																<img
																	src={api + e.id_payment + '.png'}
																	width={50}
																	height={50}
																/>
															</Button>
														</td>
													</tr>
												))
											) : (
												<div>----- ไม่มีรายการชำระเงิน -----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						รูปสลิปการชำระเงิน
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<img src={api + id_payment + '.png'} style={{ width: '435px', height: 'auto' }} />
								</Col>
							</Row>
						</Container>
					</ModalBody>
				</Modal>
			</div>
		);
	}
}
