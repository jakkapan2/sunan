import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
import { FaLock, FaUserShield, FaFileImage } from 'react-icons/fa';

let api = sever + '/image/room/';
let api_payment = sever + '/image/payment/';

export default class SaleCheckRoomReservations extends Component {
	constructor(props) {
		super(props);

		this.state = {
			booking: [],
			modal: false,
			modalDetail: {},
			payment: []
		};
		this.toggle = this.toggle.bind(this);
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		try {
			let res = await get('/get_room_detail_by_id');
			let payment = await get('/get_payment');
			this.setState({
				booking: res.result
					.filter((e) => Number(e.id_admin) === Number(userData.id))
					.sort((a, b) => b.id_booking - a.id_booking)
					.map((e) => ({
						id_meeting: e.id_meeting,
						name_meeting: e.name_meeting,
						datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
						dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
						status_booking: e.status_booking,
						id_booking: e.id_booking,
						chair_meeting: e.chair_meeting,
						location_meeting: e.location_meeting,
						//-------------------------------------------------------------------------
						datestart: moment(e.datestart_booking).format('YYYY-MM-DD'),
						dateend: moment(e.dateend_booking).format('YYYY-MM-DD'),
						timestart: e.timestart_booking,
						timeend: e.timeend_booking,
						amount_booking: e.amount_booking,
						detail_booking: e.detail_booking,
						//-----------------------------------------------------
						amount_meeting: e.amount_meeting,
						deposit: e.deposit,
						type_meeting: e.type_meeting
					})),
				payment: payment.result
			});
		} catch (error) {
			console.log(error);
		}
	}
	canCal = async (e) => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'คุณต้องการยกเลิกใช้หรือไม่!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then(async (result) => {
			if (result.value) {
				try {
					let res = await post('/cancal_booking', { id: e });
					if (res.success) {
						Swal.fire('ยกเลิกสำเร็จ', res.message, 'success').then(() => window.location.reload());
					} else {
						Swal.fire('ผิดพลาด', 'ยกเลิกไม่สำเร็จ', 'error');
					}
				} catch (error) {
					Swal.fire('ผิดพลาด', error, 'error');
				}
			}
		});
	};
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	searchText = async (e) => {
		let userData = JSON.parse(localStorage.getItem('data'));
		try {
			let res = await get('/get_room_detail_by_id');
			let payment = await get('/get_payment');
			let texts = e.toLowerCase();
			let a = res.result
				.filter((e) => Number(e.id_admin) === Number(userData.id))
				.sort((a, b) => b.id_booking - a.id_booking)
				.map((e) => ({
					id_meeting: e.id_meeting,
					name_meeting: e.name_meeting,
					datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
					dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
					status_booking: e.status_booking,
					id_booking: e.id_booking,
					chair_meeting: e.chair_meeting,
					location_meeting: e.location_meeting,
					amount_meeting: e.amount_meeting,
					deposit: e.deposit,
					type_meeting: e.type_meeting,
					a:
						e.status_booking === 'รอตรวจสอบ'
							? payment.result.length > 0 &&
								payment.result.filter((el) => el.id_booking_payment === e.id_booking).length > 0
								? payment.result
										.filter((el) => el.id_booking_payment === e.id_booking)
										.some((el) => el.type_payment === 'ชำระเงิน')
									? 'รอยืนยันการชำระเงิน'
									: 'รอยืนยันการชำระค่ามัดจำ'
								: 'ยังไม่ชำระเงิน'
							: e.status_booking === 'รอการชำระเงิน'
								? payment.result.length > 0 &&
									payment.result
										.filter((el) => el.id_booking_payment === e.id_booking)
										.some((el) => el.type_payment === 'ชำระเงิน')
									? 'รอยืนยันการชำระเงิน'
									: 'รอการชำระเงิน'
								: null
				}))
				.filter(
					(el) =>
						String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.datestart_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.dateend_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.status_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.amount_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.deposit).toLowerCase().indexOf(texts) > -1 ||
						String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.a).toLowerCase().indexOf(texts) > -1
				);
			this.setState({ booking: a });
		} catch (error) {
			console.log(error);
		}
	};
	render() {
		let { booking, modal, payment, id_payment, check } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>การชำระเงิน</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
								<div>
									<Table striped>
										<thead>
											<tr>
												<th>รูปภาพ</th>
												<th>ชื่อห้อง</th>
												<th>ประเภทห้อง</th>
												<th>ราคา</th>
												<th>ค่ามัดจำ</th>
												<th>สถานะ</th>
												<th />
												<th>สลิป</th>
											</tr>
										</thead>
										<tbody>
											{booking.length > 0 ? (
												booking.map((e, i) => (
													<tr key={i}>
														<td>
															<img
																src={api + e.id_meeting + '.png'}
																width={50}
																height={50}
															/>
														</td>
														<td>{e.name_meeting}</td>
														<td>{e.type_meeting}</td>
														<td>{e.amount_meeting.toLocaleString() + ' บาท'}</td>
														<td>{e.deposit.toLocaleString() + ' บาท'}</td>
														<td
															style={{
																color:
																	e.status_booking === 'รอตรวจสอบ'
																		? '#008ae6'
																		: e.status_booking === 'ชำระเงินสำเร็จ'
																			? '#4ce600'
																			: '#4dd2ff'
															}}
														>
															{e.status_booking}
														</td>
														<td>
															{e.status_booking === 'รอตรวจสอบ' ? payment.length > 0 &&
															payment.filter(
																(el) => el.id_booking_payment === e.id_booking
															).length > 0 ? payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																'รอยืนยันการชำระเงิน'
															) : (
																'รอยืนยันการชำระค่ามัดจำ'
															) : (
																'ยังไม่ชำระเงิน'
															) : e.status_booking === 'รอการชำระเงิน' ? payment.length >
																0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																'รอยืนยันการชำระเงิน'
															) : (
																'รอการชำระเงิน'
															) : null}
														</td>
														<td>
															{e.status_booking === 'รอตรวจสอบ' ? payment.length > 0 &&
															payment.filter(
																(el) => el.id_booking_payment === e.id_booking
															).length > 0 ? payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																<Button
																	outline
																	size="sm"
																	color="success"
																	onClick={() =>
																		this.setState(
																			{
																				id_payment: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].id_payment,
																				check: true,
																				id_booking: e.id_booking,
																				status_booking: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].type_payment
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															) : (
																<Button
																	outline
																	size="sm"
																	color="primary"
																	onClick={() =>
																		this.setState(
																			{
																				id_payment: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].id_payment,
																				check: true,
																				id_booking: e.id_booking,
																				status_booking: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].type_payment
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															) : null : e.status_booking ===
															'รอการชำระเงิน' ? payment.length > 0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																<div>
																	<Button
																		outline
																		size="sm"
																		color="primary"
																		onClick={() =>
																			this.setState(
																				{
																					id_payment: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[0].id_payment,
																					check: false,
																					status_booking: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[0].type_payment
																				},
																				() => this.toggle()
																			)}
																	>
																		<FaFileImage />
																	</Button>{' '}
																	<Button
																		outline
																		size="sm"
																		color="success"
																		onClick={() =>
																			this.setState(
																				{
																					id_payment: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[1].id_payment,
																					check: true,
																					id_booking: e.id_booking,
																					status_booking: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[1].type_payment
																				},
																				() => this.toggle()
																			)}
																	>
																		<FaFileImage />
																	</Button>
																</div>
															) : (
																<Button
																	outline
																	size="sm"
																	color="primary"
																	onClick={() =>
																		this.setState(
																			{
																				id_payment: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].id_payment,
																				check: false,
																				status_booking: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].type_payment
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															) : payment.length > 0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระค่ามัดจำ') ? (
																<div>
																	<Button
																		outline
																		size="sm"
																		color="primary"
																		onClick={() =>
																			this.setState(
																				{
																					id_payment: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[0].id_payment,
																					check: false,
																					status_booking: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[0].type_payment
																				},
																				() => this.toggle()
																			)}
																	>
																		<FaFileImage />
																	</Button>{' '}
																	<Button
																		outline
																		size="sm"
																		color="success"
																		onClick={() =>
																			this.setState(
																				{
																					id_payment: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[1].id_payment,
																					check: false,
																					status_booking: payment.filter(
																						(el) =>
																							el.id_booking_payment ===
																							e.id_booking
																					)[1].type_payment
																				},
																				() => this.toggle()
																			)}
																	>
																		<FaFileImage />
																	</Button>
																</div>
															) : (
																<Button
																	outline
																	size="sm"
																	color="success"
																	onClick={() =>
																		this.setState(
																			{
																				id_payment: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].id_payment,
																				check: false,
																				status_booking: payment.filter(
																					(el) =>
																						el.id_booking_payment ===
																						e.id_booking
																				)[0].type_payment
																			},
																			() => this.toggle()
																		)}
																>
																	<FaFileImage />
																</Button>
															)}
														</td>
													</tr>
												))
											) : (
												<div>---- ไม่มีรายการจอง ----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						สลิป{this.state.status_booking}
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<img
										src={api_payment + id_payment + '.png'}
										style={{ width: '435px', height: 'auto' }}
									/>
								</Col>
							</Row>
						</Container>
					</ModalBody>
					{check === true && (
						<ModalFooter>
							<Button onClick={this.status_booking} color="primary">
								อนุมัติ
							</Button>
						</ModalFooter>
					)}
				</Modal>
			</div>
		);
	}
	status_booking = async () => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'คุณต้องการอนุมัติใช้หรือไม่!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then(async (result) => {
			if (result.value) {
				let { id_payment, id_booking, status_booking } = this.state;
				let obj = {
					id_booking,
					status_booking: status_booking === 'ชำระค่ามัดจำ' ? 'รอการชำระเงิน' : 'ชำระเงินสำเร็จ'
				};
				try {
					let res = await post('/status_booking', obj);
					if (res.success) {
						Swal.fire(res.message, 'อนุมัติสำเร็จ', 'success').then(() => window.location.reload());
					} else {
						Swal.fire('ผิดพลาด', 'ผิดพลาด', 'error');
					}
				} catch (error) {
					Swal.fire('ผิดพลาด', error, 'error');
				}
			}
		});
	};
}
