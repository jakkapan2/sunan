import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table } from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
import ModalDetail from '../../compoment/ModalDetail';
import XlsExport from 'xlsexport';
import { FaLock, FaUserShield, FaPrint } from 'react-icons/fa';

let api = sever + '/image/room/';

export default class SaleUseRoomToday extends Component {
	constructor(props) {
		super(props);

		this.state = {
			booking: [],
			modal: false,
			modalDetail: {}
		};
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		try {
			let res = await get('/get_room_detail_by_id');
			this.setState({
				booking: res.result
					.filter((e) => Number(e.id_admin) === Number(userData.id))
					.sort((a, b) => b.id_booking - a.id_booking)
					.map((e) => ({
						id_meeting: e.id_meeting,
						name_meeting: e.name_meeting,
						datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
						dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
						status_booking: e.status_booking,
						id_booking: e.id_booking,
						chair_meeting: e.chair_meeting,
						location_meeting: e.location_meeting,
						//-------------------------------------------------------------------------
						datestart: moment(e.datestart_booking).format('YYYY-MM-DD'),
						dateend: moment(e.dateend_booking).format('YYYY-MM-DD'),
						timestart: e.timestart_booking,
						timeend: e.timeend_booking,
						amount_booking: e.amount_booking,
						detail_booking: e.detail_booking
					}))
			});
		} catch (error) {
			console.log(error);
		}
	}
	canCal = async (e) => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'คุณต้องการยกเลิกใช้หรือไม่!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then(async (result) => {
			if (result.value) {
				try {
					let res = await post('/cancal_booking', { id: e });
					if (res.success) {
						Swal.fire('ยกเลิกสำเร็จ', res.message, 'success').then(() => window.location.reload());
					} else {
						Swal.fire('ผิดพลาด', 'ยกเลิกไม่สำเร็จ', 'error');
					}
				} catch (error) {
					Swal.fire('ผิดพลาด', error, 'error');
				}
			}
		});
	};
	clickModal = async (e) => {
		let res = await post('/get_room_detail', { id: e });
		this.setState({ modalDetail: res.result[0] }, () => this.setState({ modal: true }));
	};
	searchText = async (e) => {
		let userData = JSON.parse(localStorage.getItem('data'));
		let res = await get('/get_room_detail_by_id');
		let texts = e.toLowerCase();
		let a = res.result
			.filter((e) => Number(e.id_admin) === Number(userData.id))
			.sort((a, b) => b.id_booking - a.id_booking)
			.map((e) => ({
				id_meeting: e.id_meeting,
				name_meeting: e.name_meeting,
				datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
				dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
				status_booking: e.status_booking,
				id_booking: e.id_booking,
				chair_meeting: e.chair_meeting,
				location_meeting: e.location_meeting
			}))
			.filter(
				(el) =>
					String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.datestart_booking).toLowerCase().indexOf(texts) > -1 ||
					String(el.dateend_booking).toLowerCase().indexOf(texts) > -1 ||
					String(el.status_booking).toLowerCase().indexOf(texts) > -1
			);
		this.setState({ booking: a });
	};
	exportXsl = () => {
		let { booking } = this.state;
		let download_xls = booking.map((e) => ({
			ไอดี: e.id_booking,
			ชื่อห้อง: e.name_meeting,
			วันที่เริ่มต้น: e.datestart_booking,
			วันที่สิ้นสุด: e.dateend_booking,
			สถานะ: e.status_booking
		}));
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายการจองของฉันทั้งหมด');
			xls.exportToXLS(
				'รายการจองของฉันทั้งหมด' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	render() {
		let { booking, modal, modalDetail } = this.state;
		return (
			<div>
				<ModalDetail
					check={true}
					modal={modal}
					modalDetail={modalDetail}
					close={() => this.setState({ modal: false })}
				/>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>รายการจอง</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
								<div>
									<Table striped>
										<thead>
											<tr>
												<th>รูปภาพ</th>
												<th>ชื่อห้อง</th>
												<th>วันที่เริ่มต้น </th>
												<th>วันที่สิ้นสุด</th>
												<th>สถานะ</th>
												<th>
													<Button size="sm" onClick={this.exportXsl} color="primary">
														<FaPrint />
													</Button>
												</th>
											</tr>
										</thead>
										<tbody>
											{booking.length > 0 ? (
												booking.map((e, i) => (
													<tr key={i}>
														<td>
															<img
																src={api + e.id_meeting + '.png'}
																width={50}
																height={50}
															/>
														</td>
														<td>{e.name_meeting}</td>
														<td>{e.datestart_booking}</td>
														<td>{e.dateend_booking}</td>
														<td
															style={{
																color:
																	e.status_booking === 'รอตรวจสอบ'
																		? '#008ae6'
																		: e.status_booking === 'ชำระเงินสำเร็จ'
																			? '#4ce600'
																			: '#4dd2ff'
															}}
														>
															{e.status_booking}
														</td>
														<td>
															{/* {e.status_booking === 'รอตรวจสอบ' && (
																<Button
																	onClick={() =>
																		this.props.history.push({
																			pathname: '/reservationstwo',
																			state: {
																				name_meeting: e.name_meeting,
																				id_meeting: e.id_meeting,
																				location: e.location_meeting,
																				chair_meeting: e.chair_meeting,
																				datestart: e.datestart,
																				dateend: e.dateend,
																				timestart: e.timestart,
																				timeend: e.timeend,
																				amount_booking: e.amount_booking,
																				detail_booking: e.detail_booking,
																				id_booking: e.id_booking
																			}
																		})}
																	outline
																	size="sm"
																	color="info"
																>
																	แก้ไข
																</Button>
															)}{' '} */}
															<Button
																onClick={() => this.clickModal(e.id_booking)}
																outline
																size="sm"
																color="warning"
															>
																รายละเอียด
															</Button>{' '}
															{/* {e.status_booking === 'รอตรวจสอบ' && (
																<Button
																	onClick={() => this.canCal(e.id_booking)}
																	outline
																	size="sm"
																	color="danger"
																>
																	ยกเลิก
																</Button>
															)} */}
														</td>
													</tr>
												))
											) : (
												<div>---- ไม่มีรายการจอง ----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
