import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
import { FaLock, FaUserShield, FaMoneyBill } from 'react-icons/fa';
import { slip } from '../../compoment/slip';

let api = sever + '/image/room/';

export default class CheckRoomReservations extends Component {
	constructor(props) {
		super(props);

		this.state = {
			booking: [],
			modal: false,
			modalDetail: {},
			payment: []
		};
		this.toggle = this.toggle.bind(this);
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		let res = await get('/get_room_detail_banking');
		let payment = await get('/get_payment');
		this.setState({
			booking: res.result
				.filter((e) => Number(e.id_user) === Number(userData.id))
				.sort((a, b) => b.id_booking - a.id_booking)
				.map((e) => ({
					id_meeting: e.id_meeting,
					name_meeting: e.name_meeting,
					datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
					dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
					status_booking: e.status_booking,
					id_booking: e.id_booking,
					chair_meeting: e.chair_meeting,
					location_meeting: e.location_meeting,
					//-------------------------------------------------------------------------
					datestart: moment(e.datestart_booking).format('YYYY-MM-DD'),
					dateend: moment(e.dateend_booking).format('YYYY-MM-DD'),
					timestart: e.timestart_booking,
					timeend: e.timeend_booking,
					amount_booking: e.amount_booking,
					detail_booking: e.detail_booking,
					//-----------------------------------------------------
					amount_meeting: e.amount_meeting,
					deposit: e.deposit,
					type_meeting: e.type_meeting,
					PromptPay: e.PromptPay,
					Bank: e.Bank
				})),
			payment: payment.result
		});
	}
	canCal = async (e) => {
		Swal.fire({
			title: 'Are you sure?',
			text: 'คุณต้องการยกเลิกใช้หรือไม่!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes'
		}).then(async (result) => {
			if (result.value) {
				try {
					let res = await post('/cancal_booking', { id: e });
					if (res.success) {
						Swal.fire('ยกเลิกสำเร็จ', res.message, 'success').then(() => window.location.reload());
					} else {
						Swal.fire('ผิดพลาด', 'ยกเลิกไม่สำเร็จ', 'error');
					}
				} catch (error) {
					Swal.fire('ผิดพลาด', error, 'error');
				}
			}
		});
	};
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal,
			filebase64: ''
		}));
	}
	searchText = async (e) => {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/get_room_detail_banking');
			let payment = await get('/get_payment');
			let texts = e.toLowerCase();
			let a = res.result
				.filter((e) => Number(e.id_user) === Number(userData.id))
				.sort((a, b) => b.id_booking - a.id_booking)
				.map((e) => ({
					id_meeting: e.id_meeting,
					name_meeting: e.name_meeting,
					datestart_booking: moment(e.datestart_booking).add(543, 'y').format('LLLL'),
					dateend_booking: moment(e.dateend_booking).add(543, 'y').format('LLLL'),
					status_booking: e.status_booking,
					id_booking: e.id_booking,
					chair_meeting: e.chair_meeting,
					location_meeting: e.location_meeting,
					amount_meeting: e.amount_meeting,
					deposit: e.deposit,
					type_meeting: e.type_meeting,
					PromptPay: e.PromptPay,
					Bank: e.Bank,
					a:
						e.status_booking === 'รอตรวจสอบ'
							? payment.result.length > 0 &&
								payment.result.filter((el) => el.id_booking_payment === e.id_booking).length > 0
								? payment.result
										.filter((el) => el.id_booking_payment === e.id_booking)
										.some((el) => el.type_payment === 'ชำระเงิน')
									? 'รอยืนยันการชำระเงิน'
									: 'รอยืนยันการชำระค่ามัดจำ'
								: 'ยังไม่ชำระเงิน'
							: e.status_booking === 'รอการชำระเงิน'
								? payment.result.length > 0 &&
									payment.result
										.filter((el) => el.id_booking_payment === e.id_booking)
										.some((el) => el.type_payment === 'ชำระเงิน')
									? 'รอยืนยันการชำระเงิน'
									: 'รอการชำระเงิน'
								: null
				}))
				.filter(
					(el) =>
						String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.datestart_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.dateend_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.status_booking).toLowerCase().indexOf(texts) > -1 ||
						String(el.amount_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.deposit).toLowerCase().indexOf(texts) > -1 ||
						String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.a).toLowerCase().indexOf(texts) > -1
				);
			this.setState({ booking: a });
		} catch (error) {
			console.log(error);
		}
	};
	insert_payment = async () => {
		let { filebase64, id_booking_payment, type_payment, price_payment } = this.state;
		if (!filebase64) {
			Swal.fire('คำเตือน', 'กรุณาใส่รูปภาพหลักฐานการโอน', 'warning');
		} else {
			let obj = {
				id_booking_payment,
				type_payment,
				price_payment,
				image: filebase64.split(',')[1]
			};
			try {
				let res = await post('/insert_payment', obj);
				if (res.success) {
					if (type_payment === 'ชำระค่ามัดจำ') {
						let booking = await get('/get_room_detail_banking');
						let booking_me = booking.result.filter(
							(e) => Number(e.id_booking) === Number(id_booking_payment)
						);
						let line = {
							message: `
							ชำระค่ามัดจำ
							-----------------------
							*รายการจองที่ ${id_booking_payment}
							ห้องประชุม : ${booking_me[0].name_meeting}
							ประเภท :${booking_me[0].type_meeting}
							สถานที่ตั้ง : ${booking_me[0].location_meeting}
							สถานะการจอง : ${booking_me[0].status_booking}
							ทำการชำระค่ามัดจำ
							ค่ามัดจำ :${price_payment} บาท
							-----------------------
							เข้าสู่ระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม
							http://meetingroomudru.com`,
							token: booking_me[0].token_line
						};
						await post('/noti_line', line);
					} else {
						let booking = await get('/get_room_detail_banking');
						let booking_me = booking.result.filter(
							(e) => Number(e.id_booking) === Number(id_booking_payment)
						);
						let line = {
							message: `
							ชำระค่าเช่า
							-----------------------
							*รายการจองที่ ${id_booking_payment}
							ชื่อห้องประชุม : ${booking_me[0].name_meeting}
							ประเภท :${booking_me[0].type_meeting}
							สถานที่ตั้ง : ${booking_me[0].location_meeting}
							สถานะการจอง : ${booking_me[0].status_booking}
							ทำการชำระค่าเช่า
							ค่าเช่า :${price_payment} บาท
							-----------------------
							เข้าสู่ระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม
							http://meetingroomudru.com`,
							token: booking_me[0].token_line
						};
						await post('/noti_line', line);
					}
					Swal.fire('สำเร็จ', res.message, 'success').then(() => window.location.reload());
				} else {
					Swal.fire('ผิดพลาด', 'ผิดพลาด', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
				console.log(error);
			}
		}
	};
	render() {
		let { booking, modal, payment, filebase64 } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>การชำระเงิน</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
								<div>
									<Table striped>
										<thead>
											<tr>
												<th>รูปภาพ</th>
												<th>ชื่อห้อง</th>
												<th>ประเภทห้อง</th>
												<th>ราคา</th>
												<th>ค่ามัดจำ</th>
												<th>สถานะ</th>
												<th>พร้อมเพย์</th>
												<th>บัญชีธนาคาร</th>
												<th />
												<th>ใบเสร็จ</th>
											</tr>
										</thead>
										<tbody>
											{booking.length > 0 ? (
												booking.map((e, i) => (
													<tr key={i}>
														<td>
															<img
																src={api + e.id_meeting + '.png'}
																width={50}
																height={50}
															/>
														</td>
														<td>{e.name_meeting}</td>
														<td>{e.type_meeting}</td>
														<td>{e.amount_meeting.toLocaleString() + ' บาท'}</td>
														<td>{e.deposit.toLocaleString() + ' บาท'}</td>
														<td
															style={{
																color:
																	e.status_booking === 'รอตรวจสอบ'
																		? '#008ae6'
																		: e.status_booking === 'ชำระเงินสำเร็จ'
																			? '#4ce600'
																			: '#4dd2ff'
															}}
														>
															{e.status_booking}
														</td>
														<td>{e.PromptPay}</td>
														<td>{e.Bank}</td>
														<td>
															{e.status_booking === 'รอตรวจสอบ' ? payment.length > 0 &&
															payment.filter(
																(el) => el.id_booking_payment === e.id_booking
															).length > 0 ? payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																'รอยืนยันการชำระเงิน'
															) : (
																'รอยืนยันการชำระค่ามัดจำ'
															) : (
																<div>
																	<Button
																		onClick={() =>
																			this.setState(
																				{
																					id_booking_payment: e.id_booking,
																					type_payment: 'ชำระค่ามัดจำ',
																					price_payment: e.deposit
																				},
																				() => this.toggle()
																			)}
																		outline
																		size="sm"
																		color="primary"
																	>
																		ชำระค่ามัดจำ
																	</Button>{' '}
																	<Button
																		onClick={() =>
																			this.setState(
																				{
																					id_booking_payment: e.id_booking,
																					type_payment: 'ชำระเงิน',
																					price_payment: e.amount_meeting
																				},
																				() => this.toggle()
																			)}
																		outline
																		size="sm"
																		color="info"
																	>
																		ชำระเงิน
																	</Button>
																</div>
															) : e.status_booking === 'รอการชำระเงิน' ? payment.length >
																0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																'รอยืนยันการชำระเงิน'
															) : (
																<Button
																	onClick={() =>
																		this.setState(
																			{
																				id_booking_payment: e.id_booking,
																				type_payment: 'ชำระเงิน',
																				price_payment: e.amount_meeting
																			},
																			() => this.toggle()
																		)}
																	outline
																	size="sm"
																	color="primary"
																>
																	ชำระเงิน
																</Button>
															) : null}
														</td>
														<td>
															{e.status_booking === 'รอตรวจสอบ' ? payment.length > 0 &&
															payment.filter(
																(el) => el.id_booking_payment === e.id_booking
															).length > 0 ? payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some(
																	(el) => el.type_payment === 'ชำระเงิน'
																) ? null : null : null : e.status_booking ===
															'รอการชำระเงิน' ? payment.length > 0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระเงิน') ? (
																<div>
																	<Button
																		outline
																		size="sm"
																		color="primary"
																		onClick={async () => {
																			let id = payment.filter(
																				(el) =>
																					el.id_booking_payment ===
																					e.id_booking
																			)[0].id_payment;
																			let res = await post('/bill', {
																				id_payment: id
																			});
																			slip(
																				res.result,
																				res.result.map((e) => ({
																					ชื่อห้องประชุม:
																						res.result[0].name_meeting,
																					ประเภท: res.result[0].type_meeting,
																					สถานที่ตั้ง:
																						res.result[0].location_meeting,
																					อุปกรณ์เสริม:
																						res.result[0].tool_meeting,
																					รายละเอียด:
																						res.result[0].detail_meeting,
																					จำนวนที่นั่ง:
																						res.result[0].chair_meeting +
																						' ที่นั่ง',
																					จำนวนที่เข้าร่วม:
																						res.result[0].amount_booking +
																						' คน',
																					ราคา:
																						res.result[0].amount_meeting +
																						' บาท',
																					ราคามัดจำ:
																						res.result[0].deposit + ' บาท',
																					สถานะการจอง:
																						res.result[0].status_booking,
																					วันที่จอง:
																						moment(
																							res.result[0]
																								.datestart_booking
																						)
																							.add(543, 'y')
																							.format('LLLL') +
																						' ถึง ' +
																						moment(
																							res.result[0]
																								.dateend_booking
																						)
																							.add(543, 'y')
																							.format('LLLL')
																				}))
																			);
																		}}
																	>
																		<FaMoneyBill />
																	</Button>{' '}
																	<Button
																		outline
																		size="sm"
																		color="success"
																		onClick={async () => {
																			let id = payment.filter(
																				(el) =>
																					el.id_booking_payment ===
																					e.id_booking
																			)[1].id_payment;
																			let res = await post('/bill', {
																				id_payment: id
																			});
																			slip(
																				res.result,
																				res.result.map((e) => ({
																					ชื่อห้องประชุม:
																						res.result[0].name_meeting,
																					ประเภท: res.result[0].type_meeting,
																					สถานที่ตั้ง:
																						res.result[0].location_meeting,
																					อุปกรณ์เสริม:
																						res.result[0].tool_meeting,
																					รายละเอียด:
																						res.result[0].detail_meeting,
																					จำนวนที่นั่ง:
																						res.result[0].chair_meeting +
																						' ที่นั่ง',
																					จำนวนที่เข้าร่วม:
																						res.result[0].amount_booking +
																						' คน',
																					ราคา:
																						res.result[0].amount_meeting +
																						' บาท',
																					ราคามัดจำ:
																						res.result[0].deposit + ' บาท',
																					สถานะการจอง:
																						res.result[0].status_booking,
																					วันที่จอง:
																						moment(
																							res.result[0]
																								.datestart_booking
																						)
																							.add(543, 'y')
																							.format('LLLL') +
																						' ถึง ' +
																						moment(
																							res.result[0]
																								.dateend_booking
																						)
																							.add(543, 'y')
																							.format('LLLL')
																				}))
																			);
																		}}
																	>
																		<FaMoneyBill />
																	</Button>
																</div>
															) : (
																<Button
																	outline
																	size="sm"
																	color="primary"
																	onClick={async () => {
																		let id = payment.filter(
																			(el) =>
																				el.id_booking_payment === e.id_booking
																		)[0].id_payment;
																		let res = await post('/bill', {
																			id_payment: id
																		});
																		slip(
																			res.result,
																			res.result.map((e) => ({
																				ชื่อห้องประชุม:
																					res.result[0].name_meeting,
																				ประเภท: res.result[0].type_meeting,
																				สถานที่ตั้ง:
																					res.result[0].location_meeting,
																				อุปกรณ์เสริม:
																					res.result[0].tool_meeting,
																				รายละเอียด:
																					res.result[0].detail_meeting,
																				จำนวนที่นั่ง:
																					res.result[0].chair_meeting +
																					' ที่นั่ง',
																				จำนวนที่เข้าร่วม:
																					res.result[0].amount_booking +
																					' คน',
																				ราคา:
																					res.result[0].amount_meeting +
																					' บาท',
																				ราคามัดจำ:
																					res.result[0].deposit + ' บาท',
																				สถานะการจอง:
																					res.result[0].status_booking,
																				วันที่จอง:
																					moment(
																						res.result[0].datestart_booking
																					)
																						.add(543, 'y')
																						.format('LLLL') +
																					' ถึง ' +
																					moment(
																						res.result[0].dateend_booking
																					)
																						.add(543, 'y')
																						.format('LLLL')
																			}))
																		);
																	}}
																>
																	<FaMoneyBill />
																</Button>
															) : payment.length > 0 &&
															payment
																.filter((el) => el.id_booking_payment === e.id_booking)
																.some((el) => el.type_payment === 'ชำระค่ามัดจำ') ? (
																<div>
																	<Button
																		outline
																		size="sm"
																		color="primary"
																		onClick={async () => {
																			let id = payment.filter(
																				(el) =>
																					el.id_booking_payment ===
																					e.id_booking
																			)[0].id_payment;
																			let res = await post('/bill', {
																				id_payment: id
																			});
																			slip(
																				res.result,
																				res.result.map((e) => ({
																					ชื่อห้องประชุม:
																						res.result[0].name_meeting,
																					ประเภท: res.result[0].type_meeting,
																					สถานที่ตั้ง:
																						res.result[0].location_meeting,
																					อุปกรณ์เสริม:
																						res.result[0].tool_meeting,
																					รายละเอียด:
																						res.result[0].detail_meeting,
																					จำนวนที่นั่ง:
																						res.result[0].chair_meeting +
																						' ที่นั่ง',
																					จำนวนที่เข้าร่วม:
																						res.result[0].amount_booking +
																						' คน',
																					ราคา:
																						res.result[0].amount_meeting +
																						' บาท',
																					ราคามัดจำ:
																						res.result[0].deposit + ' บาท',
																					สถานะการจอง:
																						res.result[0].status_booking,
																					วันที่จอง:
																						moment(
																							res.result[0]
																								.datestart_booking
																						)
																							.add(543, 'y')
																							.format('LLLL') +
																						' ถึง ' +
																						moment(
																							res.result[0]
																								.dateend_booking
																						)
																							.add(543, 'y')
																							.format('LLLL')
																				}))
																			);
																		}}
																	>
																		<FaMoneyBill />
																	</Button>{' '}
																	<Button
																		outline
																		size="sm"
																		color="success"
																		onClick={async () => {
																			let id = payment.filter(
																				(el) =>
																					el.id_booking_payment ===
																					e.id_booking
																			)[1].id_payment;
																			let res = await post('/bill', {
																				id_payment: id
																			});
																			slip(
																				res.result,
																				res.result.map((e) => ({
																					ชื่อห้องประชุม:
																						res.result[0].name_meeting,
																					ประเภท: res.result[0].type_meeting,
																					สถานที่ตั้ง:
																						res.result[0].location_meeting,
																					อุปกรณ์เสริม:
																						res.result[0].tool_meeting,
																					รายละเอียด:
																						res.result[0].detail_meeting,
																					จำนวนที่นั่ง:
																						res.result[0].chair_meeting +
																						' ที่นั่ง',
																					จำนวนที่เข้าร่วม:
																						res.result[0].amount_booking +
																						' คน',
																					ราคา:
																						res.result[0].amount_meeting +
																						' บาท',
																					ราคามัดจำ:
																						res.result[0].deposit + ' บาท',
																					สถานะการจอง:
																						res.result[0].status_booking,
																					วันที่จอง:
																						moment(
																							res.result[0]
																								.datestart_booking
																						)
																							.add(543, 'y')
																							.format('LLLL') +
																						' ถึง ' +
																						moment(
																							res.result[0]
																								.dateend_booking
																						)
																							.add(543, 'y')
																							.format('LLLL')
																				}))
																			);
																		}}
																	>
																		<FaMoneyBill />
																	</Button>
																</div>
															) : (
																<Button
																	outline
																	size="sm"
																	color="success"
																	onClick={async () => {
																		let id = payment.filter(
																			(el) =>
																				el.id_booking_payment === e.id_booking
																		)[0].id_payment;
																		let res = await post('/bill', {
																			id_payment: id
																		});
																		slip(
																			res.result,
																			res.result.map((e) => ({
																				ชื่อห้องประชุม:
																					res.result[0].name_meeting,
																				ประเภท: res.result[0].type_meeting,
																				สถานที่ตั้ง:
																					res.result[0].location_meeting,
																				อุปกรณ์เสริม:
																					res.result[0].tool_meeting,
																				รายละเอียด:
																					res.result[0].detail_meeting,
																				จำนวนที่นั่ง:
																					res.result[0].chair_meeting +
																					' ที่นั่ง',
																				จำนวนที่เข้าร่วม:
																					res.result[0].amount_booking +
																					' คน',
																				ราคา:
																					res.result[0].amount_meeting +
																					' บาท',
																				ราคามัดจำ:
																					res.result[0].deposit + ' บาท',
																				สถานะการจอง:
																					res.result[0].status_booking,
																				วันที่จอง:
																					moment(
																						res.result[0].datestart_booking
																					)
																						.add(543, 'y')
																						.format('LLLL') +
																					' ถึง ' +
																					moment(
																						res.result[0].dateend_booking
																					)
																						.add(543, 'y')
																						.format('LLLL')
																			}))
																		);
																	}}
																>
																	<FaMoneyBill />
																</Button>
															)}
														</td>
													</tr>
												))
											) : (
												<div>---- ไม่มีรายการชำระเงิน ----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						{this.state.type_payment}
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<Input
										type="file"
										onChange={this.uploadImg}
										accept="image/x-png,image/gif,image/jpeg"
									/>
									{filebase64 && <img src={filebase64} style={{ width: '435px', height: 'auto' }} />}
								</Col>
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={this.insert_payment} color="primary">
							ตกลง
						</Button>
						<Button onClick={this.toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
}
