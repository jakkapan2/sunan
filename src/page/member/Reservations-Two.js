import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label } from 'reactstrap';
import './Reservations.css';
import Swal from 'sweetalert2';
import { get, post } from '../../service/service';

export default class ReservationsTwo extends Component {
	constructor(props) {
		super(props);
		let { datestart, dateend, timestart, timeend, amount_booking, detail_booking } = this.props.location.state;

		this.state = {
			// tool: [],
			// tool_name: []
			amount_booking: amount_booking,
			dateend_booking: dateend,
			datestart_booking: datestart,
			detail_booking: detail_booking,
			timeend_booking: timeend,
			timestart_booking: timestart
		};
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		// this.get_tool();
		let userData = JSON.parse(localStorage.getItem('data'));
		if (userData) {
			let res = await post('/getuser', { id: userData.id });
			this.setState({
				name: res.result[0].fname + ' ' + res.result[0].lname
			});
		}
	}
	// get_tool = async () => {
	// 	let res = await get('/tool');
	// 	this.setState({ tool: res.result });
	// };
	onChangs = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	// onChangsBox = (e, i) => {
	// 	let { tool_name } = this.state;
	// 	if (e.target.checked) {
	// 		tool_name.push(e.target.name);
	// 		this.setState({ tool_name });
	// 	} else {
	// 		tool_name.splice(i, 1);
	// 		this.setState({ tool_name });
	// 	}
	// };
	Submit = async () => {
		let {
			amount_booking,
			dateend_booking,
			datestart_booking,
			detail_booking,
			timeend_booking,
			timestart_booking
			// tool_name
		} = this.state;
		let obj = {
			amount_booking,
			dateend_booking: dateend_booking + ' ' + timeend_booking,
			datestart_booking: datestart_booking + ' ' + timestart_booking,
			detail_booking,
			timeend_booking,
			timestart_booking,
			// tool_name: String(tool_name),
			id: Number(this.props.location.state.id_booking)
		};
		if (
			!amount_booking ||
			!dateend_booking ||
			!datestart_booking ||
			!detail_booking ||
			!timeend_booking ||
			!timestart_booking
		) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			try {
				let res = await post('/updated_booking', obj);
				if (res.success) {
					Swal.fire('สำเร็จ', 'แก้ไขรายการจองเสร็จสิ้น', 'success').then(() =>
						this.props.history.push('/userroomtoday')
					);
				} else {
					Swal.fire('ผิดพลาด', 'ไม่สามารถแก้ไขรายการจองได้', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
			}
		}
	};
	render() {
		let {
			name,
			amount_booking,
			dateend_booking,
			datestart_booking,
			detail_booking,
			timeend_booking,
			timestart_booking
		} = this.state;
		let { name_meeting, location, chair_meeting } = this.props.location.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>รายระเอียดห้องประชุม</div>
								</div>
								<Form style={{ paddingTop: '20px' }}>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											ชื่อผู้จอง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={name} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											ห้องประชุมที่จอง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={name_meeting} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											สถานที่/ที่ตั้ง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={location} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนที่นั้ง
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												disabled
												value={Number(chair_meeting).toLocaleString()}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													วันที่เริ่มต้น
												</Label>
												<Col sm={6}>
													<Input
														value={datestart_booking}
														type="date"
														name="datestart_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>

										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													วันที่สิ้นสุด
												</Label>
												<Col sm={6}>
													<Input
														value={dateend_booking}
														type="date"
														name="dateend_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													เวลาเริ่มต้น
												</Label>
												<Col sm={6}>
													<Input
														value={timestart_booking}
														type="time"
														name="timestart_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													เวลาสิ้นสุด
												</Label>
												<Col sm={6}>
													<Input
														value={timeend_booking}
														type="time"
														name="timeend_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนคนที่จะเข้าร่วม
										</Label>
										<Col sm={10}>
											<Input
												value={amount_booking}
												type="number"
												name="amount_booking"
												placeholder="จำนวนคนที่จะเข้าร่วม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									{/* <FormGroup row>
										<Label for="exampleText" sm={2}>
											อุปกรณ์เสริม
										</Label>
										<Col sm={10}>
											<FormGroup check>
												{tool.length > 0 &&
													tool.map((e, i) => (
														<Label key={i} style={{ marginRight: '1rem' }}>
															<input
																type="checkbox"
																name={e.name_tool}
																onChange={(e) => this.onChangsBox(e, i)}
																style={{ marginRight: '0.2rem' }}
															/>
															<span>{e.name_tool}</span>
														</Label>
													))}
											</FormGroup>
										</Col>
									</FormGroup> */}
									<FormGroup row>
										<Label for="exampleText" sm={2}>
											รายระเอียดการใช้งาน
										</Label>
										<Col sm={10}>
											<Input
												value={detail_booking}
												type="textarea"
												name="detail_booking"
												id="exampleText"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup check row>
										<Col sm={{ size: 20, offset: 6 }}>
											<Button
												color="primary"
												onClick={this.Submit}
												style={{ marginRight: '3rem' }}
											>
												ตกลง
											</Button>
											<Button
												color="danger"
												onClick={() => this.props.history.push('/userroomtoday')}
											>
												ยกเลิก
											</Button>
										</Col>
									</FormGroup>
								</Form>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
