import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Input,
  Button,
  Table,
  Form,
  FormGroup,
  Label
} from "reactstrap";

export default class CheckRoom extends Component {
  async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
  render() {
    return (
      <div>
        <Container style={{ paddingTop: "1rem" }}>
          <Row>
            <Col xs={12}>
              <div>
                <div id="cssheader">
                  <div style={{ paddingRight: "10px", color: "#fff" }}>
                    ประวัติการจองห้องประชุม
                  </div>
                </div>
                <div>
                  <Table striped>
                    <thead>
                      <tr>
                        <th>ลำดับที่ </th>
                        <th>ชื่อผู้จอง</th>
                        <th>ชื่อห้องประชุม</th>
                        <th>วันที่จอง</th>
                        <th>เวลาเริ่มต้น</th>
                        <th>เวลาวิ้นสุด</th>
                        <th>รายระเอียด</th>
                        <th>จำนวน(คน)</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th>1</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>12/08/19</th>
                        <th>10.00pm</th>
                        <th>12.00pm</th>
                        <th>สัมนา</th>
                        <th>100 คน</th>
                      </tr>
                      <tr>
                        <th>2</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>12/08/19</th>
                        <th>10.00pm</th>
                        <th>12.00pm</th>
                        <th>สัมนา</th>
                        <th>100 คน</th>
                      </tr>
                      <tr>
                        <th>3</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>12/08/19</th>
                        <th>10.00pm</th>
                        <th>12.00pm</th>
                        <th>สัมนา</th>
                        <th>100 คน</th>
                      </tr>
                      <tr>
                        <th scope="row">4</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                      </tr>
                    </tbody>
                  </Table>
                </div>
                <div>
                  <Form inline>
                    <Col xs={4} />
                  </Form>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
