import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label } from 'reactstrap';
import './Reservations.css';
import Swal from 'sweetalert2';
import { get, post } from '../../service/service';
import moment from 'moment';

export default class Reservations extends Component {
	constructor(props) {
		super(props);

		this.state = {
			// tool: [],
			// tool_name: []
		};
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		// this.get_tool();
		let userData = JSON.parse(localStorage.getItem('data'));
		if (userData) {
			let res = await post('/getuser', { id: userData.id });
			this.setState({
				name: res.result[0].fname + ' ' + res.result[0].lname
			});
		}
	}
	// get_tool = async () => {
	// 	let res = await get('/tool');
	// 	this.setState({ tool: res.result });
	// };
	onChangs = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	// onChangsBox = (e, i) => {
	// 	let { tool_name } = this.state;
	// 	if (e.target.checked) {
	// 		tool_name.push(e.target.name);
	// 		this.setState({ tool_name });
	// 	} else {
	// 		tool_name.splice(i, 1);
	// 		this.setState({ tool_name });
	// 	}
	// };
	Submit = async () => {
		let userData = JSON.parse(localStorage.getItem('data'));
		let {
			amount_booking,
			dateend_booking,
			datestart_booking,
			detail_booking,
			timeend_booking,
			timestart_booking
			// tool_name
		} = this.state;
		let obj = {
			amount_booking,
			dateend_booking: dateend_booking + ' ' + timeend_booking + ':00',
			datestart_booking: datestart_booking + ' ' + timestart_booking + ':00',
			detail_booking,
			timeend_booking,
			timestart_booking,
			// tool_name: String(tool_name),
			id_user: Number(userData.id),
			id_list_meeting: Number(this.props.location.state.id_meeting)
		};
		if (
			!amount_booking ||
			!dateend_booking ||
			!datestart_booking ||
			!detail_booking ||
			!timeend_booking ||
			!timestart_booking
		) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			try {
				// console.log('obj', obj);
				let state_stame = moment(obj.datestart_booking).valueOf();
				let end_stame = moment(obj.dateend_booking).valueOf();
				// console.log('state_stame', state_stame, 'end_stame', end_stame);
				let getbooking = await get('/get_booking');
				let gendate = getbooking.result
					.filter((e) => Number(e.id_list_meeting) === obj.id_list_meeting)
					.map((e) => ({
						date_s: moment(e.datestart_booking).valueOf(),
						date_e: moment(e.dateend_booking).valueOf()
					}))
					.filter(
						(e) =>
							(state_stame >= e.date_s && state_stame <= e.date_e) ||
							(end_stame >= e.date_s && end_stame <= e.date_e)
					);
				// console.log(
				// 	'object',
				// 	getbooking.result.filter((e) => Number(e.id_list_meeting) === obj.id_list_meeting).map((e) => ({
				// 		date_s: moment(e.datestart_booking).valueOf(),
				// 		date_e: moment(e.dateend_booking).valueOf()
				// 	}))
				// );
				// console.log('gendate', gendate);
				if (gendate.length > 0) {
					Swal.fire('การจองล้มเหลว', 'วันเวลาที่จองซ้ำ', 'warning');
				} else {
					let res = await post('/insert_booking', obj);
					if (res.success) {
						let booking = await get('/get_booking');
						let booking_me = booking.result.filter((e) => Number(e.id_booking) === Number(res.id));
						let line = {
							message: `
								รายละเอียดการจองของฉัน
								-----------------------
								ชื่อห้องประชุม : ${booking_me[0].name_meeting}
								ประเภท :${booking_me[0].type_meeting}
								สถานที่ตั้ง : ${booking_me[0].location_meeting}
								อุปกรณ์เสริม : ${booking_me[0].tool_meeting}
								รายละเอียด :${booking_me[0].detail_meeting}
								จำนวนที่นั่ง :${booking_me[0].chair_meeting} ที่นั่ง
								จำนวนที่เข้าร่วม :${booking_me[0].amount_booking} คน
								ราคา :${booking_me[0].amount_meeting} บาท
								ราคามัดจำ :${booking_me[0].deposit} บาท
								สถานะการจอง : ${booking_me[0].status_booking}
								วันที่จอง : ${moment(booking_me[0].datestart_booking).add(543, 'y').format('LLLL') +
									' ถึง ' +
									moment(booking_me[0].dateend_booking).add(543, 'y').format('LLLL')}
								-----------------------
								เข้าสู่ระบบเพื่อตรวจสอบข้อมูลเพิ่มเติม
								http://meetingroomudru.com`,
							token: booking_me[0].token_line
						};
						await post('/noti_line', line);
						Swal.fire(
							'สำเร็จ',
							'จองห้องประชุมเสร็จสิ้น ติดตามสถานะได้ทางแจ้งเตือน Line',
							'success'
						).then(() => this.props.history.push('/calendar'));
					} else {
						Swal.fire('ผิดพลาด', 'ไม่สามารถจองห้องประชุมได้', 'error');
					}
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
			}
		}
	};
	render() {
		let { name } = this.state;
		let { name_meeting, location, chair_meeting } = this.props.location.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>รายระเอียดห้องประชุม</div>
								</div>
								<Form style={{ paddingTop: '20px' }}>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											ชื่อผู้จอง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={name} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											ห้องประชุมที่จอง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={name_meeting} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											สถานที่/ที่ตั้ง
										</Label>
										<Col sm={10}>
											<Input type="text" disabled value={location} />
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนที่นั้ง
										</Label>
										<Col sm={10}>
											<Input
												type="text"
												disabled
												value={Number(chair_meeting).toLocaleString()}
											/>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													วันที่เริ่มต้น
												</Label>
												<Col sm={6}>
													<Input
														type="date"
														name="datestart_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>

										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													วันที่สิ้นสุด
												</Label>
												<Col sm={6}>
													<Input
														type="date"
														name="dateend_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													เวลาเริ่มต้น
												</Label>
												<Col sm={6}>
													<Input
														type="time"
														name="timestart_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
										<Col sm={6}>
											<FormGroup row>
												<Label for="examplePassword" sm={4}>
													เวลาสิ้นสุด
												</Label>
												<Col sm={6}>
													<Input
														type="time"
														name="timeend_booking"
														onChange={this.onChangs}
													/>
												</Col>
											</FormGroup>
										</Col>
									</FormGroup>
									<FormGroup row>
										<Label for="examplePassword" sm={2}>
											จำนวนคนที่จะเข้าร่วม
										</Label>
										<Col sm={10}>
											<Input
												type="number"
												name="amount_booking"
												placeholder="จำนวนคนที่จะเข้าร่วม"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									{/* <FormGroup row>
										<Label for="exampleText" sm={2}>
											อุปกรณ์เสริม
										</Label>
										<Col sm={10}>
											<FormGroup check>
												{tool.length > 0 &&
													tool.map((e, i) => (
														<Label key={i} style={{ marginRight: '1rem' }}>
															<input
																type="checkbox"
																name={e.name_tool}
																onChange={(e) => this.onChangsBox(e, i)}
																style={{ marginRight: '0.2rem' }}
															/>
															<span>{e.name_tool}</span>
														</Label>
													))}
											</FormGroup>
										</Col>
									</FormGroup> */}
									<FormGroup row>
										<Label for="exampleText" sm={2}>
											รายระเอียดการใช้งาน
										</Label>
										<Col sm={10}>
											<Input
												type="textarea"
												name="detail_booking"
												id="exampleText"
												onChange={this.onChangs}
											/>
										</Col>
									</FormGroup>
									<FormGroup check row>
										<Col sm={{ size: 20, offset: 6 }}>
											<Button
												color="primary"
												onClick={this.Submit}
												style={{ marginRight: '3rem' }}
											>
												ตกลง
											</Button>
											<Button
												color="danger"
												onClick={() => this.props.history.push('/listlogin')}
											>
												ยกเลิก
											</Button>
										</Col>
									</FormGroup>
								</Form>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
