import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Input,
  Button,
  Table,
  Form,
  FormGroup,
  Label
} from "reactstrap";

export default class Editprofile extends Component {
  async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
  render() {
    return (
      <div>
        <Container style={{ paddingTop: "1rem" }}>
          <Row>
            <Col xs={12}>
              <div>
                <div id="cssheader">
                  <div style={{ paddingRight: "10px", color: "#fff" }}>
                    แก้ไขข้อมูลผู้ใช้งาน
                  </div>
                </div>
                <div style={{ paddingTop: "20px" }}>
                  <FormGroup row>
                    <Label for="exampleEmail" sm={2}>
                      ชื่อผู้ใช้งาน
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="email"
                        name="email"
                        id="exampleEmail"
                        placeholder="with a placeholder"
                      />
                    </Col>
                  </FormGroup>
                </div>
                <div>
                  <FormGroup row>
                    <Label for="examplePassword" sm={2}>
                      ชื่อ - สกุล
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="password"
                        name="password"
                        id="examplePassword"
                        placeholder="password placeholder"
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="examplePassword" sm={2}>
                      รหัสผ่าน
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="password"
                        name="password"
                        id="examplePassword"
                        placeholder="password placeholder"
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="exampleEmail" sm={2}>
                      ยืนยันรหัสผ่าน
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="email"
                        name="email"
                        id="exampleEmail"
                        placeholder="with a placeholder"
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="examplePassword" sm={2}>
                      อีเมลย์
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="password"
                        name="password"
                        id="examplePassword"
                        placeholder="password placeholder"
                      />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="examplePassword" sm={2}>
                      เบอร์โทร
                    </Label>
                    <Col sm={10}>
                      <Input
                        type="password"
                        name="password"
                        id="examplePassword"
                        placeholder="password placeholder"
                      />
                    </Col>
                  </FormGroup>
                </div>
                <div>
                  <Form inline>
                    <Col xs={4} />
                  </Form>
                  <div
                    style={{
                      paddingTop: "10px",
                      display: "flex",
                      justifyContent: "center"
                    }}
                  >
                    <Button>ตกลง</Button>
                    <Button>ยกเลิก</Button>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
