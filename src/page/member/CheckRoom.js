import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
import { FaLock, FaUserShield, FaFileImage } from 'react-icons/fa';
let api = sever + '/image/room/';

export default class CheckRoom extends Component {
	constructor(props) {
		super(props);

		this.state = {
			list_meeting: [],
			modal: false,
			PromptPay: '',
			Bank: ''
		};
		this.toggle = this.toggle.bind(this);
	}

	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/list_meeting');
			let blacking = await get('/getblanking');
			this.setState({
				list_meeting: res.result.filter((e) => Number(e.id_admin) === Number(userData.id)),
				PromptPay: blacking.result.filter((e) => e.role === 'admin')[0].PromptPay,
				Bank: blacking.result.filter((e) => e.role === 'admin')[0].Bank
			});
		} catch (error) {
			console.log(error);
		}
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal,
			filebase64: ''
		}));
	}
	searchText = async (e) => {
		try {
			let userData = JSON.parse(localStorage.getItem('data'));
			let res = await get('/list_meeting');
			let texts = e.toLowerCase();
			let a = res.result
				.filter((e) => Number(e.id_admin) === Number(userData.id))
				.map((e) => ({
					...e,
					status_meetings:
						e.status_meeting === 'on'
							? 'ชำระเงินสำเร็จ'
							: e.status_meeting === 'wait' ? 'รอการยืนยัน' : 'รอชำระเงิน'
				}))
				.filter(
					(el) =>
						String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.location_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
						String(el.status_meetings).toLowerCase().indexOf(texts) > -1
				);
			this.setState({ list_meeting: a });
		} catch (error) {
			console.log(error);
		}
	};
	render() {
		let { list_meeting, modal, filebase64, PromptPay, Bank } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>การลงทะเบียน</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
								<br />
								<div style={{ display: 'flex', justifyContent: 'space-between' }}>
									<Button outline color="primary">
										<strong>พร้อมเพย์ : {PromptPay}</strong>
									</Button>
									<Button outline color="primary">
										<strong>บัญชีธนาคาร : {Bank}</strong>
									</Button>
								</div>
								<br />
								<div>
									<Table striped>
										<thead>
											<tr>
												<th>รูปภาพ </th>
												<th>ชื่อห้องประชุม</th>
												<th>ประเภท</th>
												<th>สถานที่ตั้ง</th>
												<th>สถานะ</th>
												<th>ราคาลงทะเบียน</th>
												<th />
											</tr>
										</thead>
										<tbody>
											{list_meeting.length > 0 ? (
												list_meeting.map((e) => (
													<tr>
														<td>
															<img
																src={api + e.id_meeting + '.png'}
																width={50}
																height={50}
															/>
														</td>
														<td>{e.name_meeting}</td>
														<td>{e.type_meeting}</td>
														<td>{e.location_meeting}</td>
														<td
															style={{
																color:
																	e.status_meeting === 'on'
																		? 'green'
																		: e.status_meeting === 'wait' ? 'blue' : 'red'
															}}
														>
															{e.status_meeting === 'on' ? (
																'ชำระเงินสำเร็จ'
															) : e.status_meeting === 'wait' ? (
																'รอการยืนยัน'
															) : (
																'รอชำระเงิน'
															)}
														</td>
														<td>{e.servicerate.toLocaleString()} บาท</td>
														<td>
															{e.status_meeting === 'off' && (
																<Button
																	size="sm"
																	onClick={() =>
																		this.setState(
																			{
																				id_m_regis: e.id_meeting,
																				price_regis: e.servicerate
																			},
																			() => this.toggle()
																		)}
																	color="success"
																>
																	ชำระเงิน
																</Button>
															)}
														</td>
													</tr>
												))
											) : (
												<div>---- ไม่มีรายการลงทะเบียน -----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						ชำระการลงทะเบียน
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<Input
										type="file"
										onChange={this.uploadImg}
										accept="image/x-png,image/gif,image/jpeg"
									/>
									{filebase64 && <img src={filebase64} style={{ width: '435px', height: 'auto' }} />}
								</Col>
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={this.insert_registration} color="primary">
							ตกลง
						</Button>
						<Button onClick={this.toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
	uploadImg = (event) => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (e) => {
				this.setState({ filebase64: e.target.result });
				// console.log('e.target.result', e.target.result);
			};
			reader.readAsDataURL(event.target.files[0]);
		}
	};
	insert_registration = async () => {
		let { filebase64, id_m_regis, price_regis } = this.state;
		if (!filebase64) {
			Swal.fire('คำเตือน', 'กรุณาใส่รูปภาพหลักฐานการโอน', 'warning');
		} else {
			let obj = {
				image: filebase64.split(',')[1],
				id_m_regis,
				price_regis
			};
			try {
				let res = await post('/insert_registration', obj);
				if (res.success) {
					await post('/status_meeting', {
						id_meeting: id_m_regis,
						status_meeting: 'wait'
					});
					Swal.fire('สำเร็จ', res.message, 'success').then(() => window.location.reload());
				} else {
					Swal.fire('ผิดพลาด', 'ผิดพลาด', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
				console.log(error);
			}
		}
	};
}
