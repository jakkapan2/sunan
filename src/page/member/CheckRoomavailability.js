import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Form,
	FormGroup,
	Label
} from 'reactstrap';
import { get, post, sever } from '../../service/service';
import Swal from 'sweetalert2';
import moment from 'moment';
let api = sever + '/image/payment/';

export default class CheckRoomavailability extends Component {
	constructor(props) {
		super(props);

		this.state = {
			payment: [],
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	async componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		let payment = await get('/get_payment');
		this.setState({
			payment: payment.result.filter((e) => Number(e.id_user) === Number(userData.id)).map((e) => ({
				name_meeting: e.name_meeting,
				timestam: moment(e.timestam).add(543, 'y').format('LLLL'),
				type_payment: e.type_payment,
				price_payment: e.price_payment,
				id_payment: e.id_payment
			}))
		});
	}
	searchText = async (e) => {
		let userData = JSON.parse(localStorage.getItem('data'));
		let payments = await get('/get_payment');
		let texts = e.toLowerCase();
		let a = payments.result
			.filter((e) => Number(e.id_user) === Number(userData.id))
			.sort((a, b) => moment(b.timestam) - moment(a.timestam))
			.map((e) => ({
				name_meeting: e.name_meeting,
				timestam: moment(e.timestam).add(543, 'y').format('LLLL'),
				type_payment: e.type_payment,
				price_payment: e.price_payment,
				id_payment: e.id_payment
			}))
			.filter(
				(el) =>
					String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.timestam).toLowerCase().indexOf(texts) > -1 ||
					String(el.type_payment).toLowerCase().indexOf(texts) > -1 ||
					String(el.price_payment).toLowerCase().indexOf(texts) > -1
			);
		this.setState({ payment: a });
	};
	render() {
		let { payment, modal, id_payment } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>ประวัติการชำระเงิน</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>

								<div>
									<Table striped>
										<thead>
											<tr>
												<th>ชื่อห้องประชุม</th>
												<th>การชำระ</th>
												<th>เป็นเงิน</th>
												<th>วันเวลาที่ชำระ</th>
												<th>ภาพประกอบ</th>
											</tr>
										</thead>
										<tbody>
											{payment.length > 0 ? (
												payment.map((e) => (
													<tr>
														<td>{e.name_meeting}</td>
														<td>{e.type_payment}</td>
														<td>{e.price_payment}</td>
														<td>{e.timestam}</td>
														<td>
															<Button
																onClick={() => {
																	this.toggle();
																	this.setState({ id_payment: e.id_payment });
																}}
															>
																<img
																	src={api + e.id_payment + '.png'}
																	width={50}
																	height={50}
																/>
															</Button>
														</td>
													</tr>
												))
											) : (
												<div>----- ไม่มีรายการชำระเงิน -----</div>
											)}
										</tbody>
									</Table>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						รูปสลิปการชำระเงิน
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<img src={api + id_payment + '.png'} style={{ width: '435px', height: 'auto' }} />
								</Col>
							</Row>
						</Container>
					</ModalBody>
				</Modal>
			</div>
		);
	}
}
