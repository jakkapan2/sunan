import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Form, FormGroup, Label } from 'reactstrap';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';
import { post, get } from '../../service/service';
import moment from 'moment';
import Swal from 'sweetalert2';
import ModalDetail from '../../compoment/ModalDetail';

export default class UseRoomCalendarTwo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: [],
			modal: false,
			modalDetail: {},
			room_meeting: []
		};
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		let res = await get('/get_booking');
		let room = await get('/room_meeting');
		this.setState({
			events: res.result.map((e) => ({
				id: e.id_booking,
				title:
					e.timestart_booking + ' - ' + e.timeend_booking + ' ' + e.name_meeting + ' ' + e.location_meeting,
				start: moment(e.datestart_booking).format('YYYY-MM-DD'),
				end: moment(e.dateend_booking).add(1, 'd').format('YYYY-MM-DD'),
				color:
					e.status_booking === 'รอตรวจสอบ'
						? '#008ae6'
						: e.status_booking === 'รอการชำระเงิน' ? '#4dd2ff' : '#4ce600',
				textColor: '#fff'
			})),
			room_meeting: room.result
		});
	}
	clickModal = async (e) => {
		let res = await post('/get_room_detail', { id: e });
		this.setState({ modalDetail: res.result[0] }, () => this.setState({ modal: true }));
	};
	changRoom = async (e) => {
		let id = e.target.value;
		if (Number(id) === 0) {
			let res = await get('/get_booking');
			this.setState({
				events: res.result.map((e) => ({
					id: e.id_booking,
					title: e.timestart_booking + ' - ' + e.timeend_booking,
					start: moment(e.datestart_booking).format('YYYY-MM-DD'),
					end: moment(e.dateend_booking).add(1, 'd').format('YYYY-MM-DD'),
					color:
						e.status_booking === 'รอตรวจสอบ'
							? '#008ae6'
							: e.status_booking === 'รอการชำระเงิน' ? '#4dd2ff' : '#4ce600',
					textColor: '#fff'
				}))
			});
		} else {
			let res = await get('/get_booking');
			this.setState({
				events: res.result.filter((e) => Number(e.id_list_meeting) === Number(id)).map((e) => ({
					id: e.id_booking,
					title: e.timestart_booking + ' - ' + e.timeend_booking,
					start: moment(e.datestart_booking).format('YYYY-MM-DD'),
					end: moment(e.dateend_booking).add(1, 'd').format('YYYY-MM-DD'),
					color:
						e.status_booking === 'รอตรวจสอบ'
							? '#008ae6'
							: e.status_booking === 'รอการชำระเงิน' ? '#4dd2ff' : '#4ce600',
					textColor: '#fff'
				}))
			});
		}
	};
	render() {
		let { events, modal, modalDetail, room_meeting } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<ModalDetail
						modal={modal}
						modalDetail={modalDetail}
						close={() => this.setState({ modal: false })}
					/>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>
										แสดงการใช้งานปฏิทินห้องประชุม
									</div>
									<div>
										<Input type="select" name="select" onChange={this.changRoom}>
											<option value={0}>ทั้งหมด</option>
											{room_meeting.length > 0 &&
												room_meeting.map((e, i) => (
													<option key={i} value={e.id_meeting}>
														{e.name_meeting}
													</option>
												))}
										</Input>
									</div>
								</div>
								<div id="example-component">
									<FullCalendar
										id="your-custom-ID"
										header={{
											left: 'prev,next today myCustomButton',
											center: 'title',
											right: 'month,basicWeek,basicDay'
										}}
										navLinks={true} // can click day/week names to navigate views
										editable={true}
										eventLimit={true} // allow "more" link when too many events
										events={events.length > 0 && events}
										eventClick={(calEvent, jsEvent, view, resourceObj) =>
											this.clickModal(calEvent.id)}
									/>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
