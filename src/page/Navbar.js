import React from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Row,
	Col,
	Input,
	Label
} from 'reactstrap';
import './navbar.css';
import ModalRegister from '../compoment/ModalRegister';
import ModalLogin from '../compoment/ModalLogin';
import ModalService from '../compoment/ModalService';
import ModalEdit from '../compoment/ModalEdit';
import { get, post } from '../service/service';

export default class Navbars extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false,
			modal: false,
			rate: false
		};
		this.toggle = this.toggle.bind(this);
		this.toggle2 = this.toggle2.bind(this);
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}
	toggle2() {
		this.setState({
			modal: !this.state.modal
		});
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (userData) {
			let res = await post('/getuser', { id: userData.id });
			this.setState({
				fname: res.result[0].fname,
				lname: res.result[0].lname,
				role: userData.role
			});
		}
	}
	render() {
		let { fname, lname, modal, role, rate } = this.state;
		if (role === 'renter') {
			return (
				<div>
					<ModalEdit modal={modal} toggle={this.toggle2} />
					<Navbar id="back-img" expand="md">
						<NavbarBrand href="/calendar">
							<text id="hover-text">Meeting Room System Online</text>
						</NavbarBrand>
						<NavbarToggler />
						<Collapse isOpen={this.state.isOpen} navbar>
							<Nav className="ml-auto" navbar>
								<DropdownToggle style={{ color: '#fff', cursor: 'default' }} nav>
									{(fname || lname) && <text>สวัสดี ​{fname + ' ' + lname}</text>}
								</DropdownToggle>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav caret>
										<text id="hover-text">เพิ่มเติม</text>
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem href="/calendar">Home</DropdownItem>
										<DropdownItem href="/listlogin">จองห้องประชุม</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/userroomtoday">รายการจองของฉัน</DropdownItem>
										<DropdownItem href="/checkroomreservations">การชำระเงิน</DropdownItem>
										<DropdownItem href="/checkRoomavailability">ประวัติการชำระเงิน</DropdownItem>
										<DropdownItem onClick={this.toggle2}>แก้ไขข้อมูลส่วนตัว</DropdownItem>
										<DropdownItem divider />
										<DropdownItem onClick={() => localStorage.clear()} href="/">
											ออกจากระบบ
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Nav>
						</Collapse>
					</Navbar>
				</div>
			);
		} else if (role === 'lessor') {
			return (
				<div>
					<ModalEdit modal={modal} toggle={this.toggle2} />
					<Navbar id="back-img" expand="md">
						<NavbarBrand href="/SaleHomeTwo">
							<text id="hover-text">Meeting Room System Online</text>
						</NavbarBrand>
						<NavbarToggler />
						<Collapse isOpen={this.state.isOpen} navbar>
							<Nav className="ml-auto" navbar>
								<DropdownToggle style={{ color: '#fff', cursor: 'default' }} nav>
									{(fname || lname) && <text>สวัสดี ​{fname + ' ' + lname}</text>}
								</DropdownToggle>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav caret>
										<text id="hover-text">เพิ่มเติม</text>
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem href="/SaleHomeTwo">ห้องประชุม</DropdownItem>
										<DropdownItem href="/addroom">เพิ่มห้องประชุม</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/SaleUseRoomToday">การจอง</DropdownItem>
										<DropdownItem href="/SaleCheckRoomReservations">การชำระเงินการจอง</DropdownItem>
										<DropdownItem href="/CheckRoom">การชำระเงินการลงทะเบียน</DropdownItem>
										<DropdownItem href="/SaleCheckRoomavailability">
											ประวัติการชำระเงินการลงทะเบียน
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/Rate">ผลการประเมินความพึงพอใจ</DropdownItem>
										<DropdownItem onClick={this.toggle2}>แก้ไขข้อมูลส่วนตัว</DropdownItem>
										<DropdownItem divider />
										<DropdownItem onClick={() => localStorage.clear()} href="/">
											ออกจากระบบ
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Nav>
						</Collapse>
					</Navbar>
				</div>
			);
		} else {
			return (
				<div>
					<ModalService modal={rate} toggle={() => this.setState({ rate: false })} />
					<ModalEdit modal={modal} toggle={this.toggle2} />
					<Navbar id="back-img" expand="md">
						<NavbarBrand href="/SaleHomeTwo">
							<text id="hover-text">Meeting Room System Online</text>
						</NavbarBrand>
						<NavbarToggler />
						<Collapse isOpen={this.state.isOpen} navbar>
							<Nav className="ml-auto" navbar>
								<DropdownToggle style={{ color: '#fff', cursor: 'default' }} nav>
									{(fname || lname) && <text>สวัสดี ​{fname + ' ' + lname}</text>}
								</DropdownToggle>
								<UncontrolledDropdown nav inNavbar>
									<DropdownToggle nav caret>
										<text id="hover-text">เพิ่มเติม</text>
									</DropdownToggle>
									<DropdownMenu right>
										<DropdownItem href="/AdminRate">Home</DropdownItem>
										{/* <DropdownItem href="/addroom">เพิ่มห้องประชุม</DropdownItem> */}
										<DropdownItem divider />
										{/* <DropdownItem href="/SaleUseRoomToday">การจอง</DropdownItem>
										<DropdownItem href="/SaleCheckRoomReservations">การชำระเงินการจอง</DropdownItem>
										<DropdownItem href="/CheckRoom">การชำระเงินการลงทะเบียน</DropdownItem> */}
										<DropdownItem href="/AdminCheckRoom">สถานะการชำระค่าลงทะเบียน</DropdownItem>
										<DropdownItem onClick={() => this.setState({ rate: true })}>
											อัตราค่าบริการลงทะเบียน
										</DropdownItem>
										<DropdownItem divider />
										<DropdownItem href="/AdminRate">ผลการประเมินความพึงพอใจ</DropdownItem>
										<DropdownItem onClick={this.toggle2}>แก้ไขข้อมูลส่วนตัว</DropdownItem>
										<DropdownItem divider />
										<DropdownItem onClick={() => localStorage.clear()} href="/">
											ออกจากระบบ
										</DropdownItem>
									</DropdownMenu>
								</UncontrolledDropdown>
							</Nav>
						</Collapse>
					</Navbar>
				</div>
			);
		}
	}
}
