import React, { Component } from 'react';
import {
	Container,
	Row,
	Col,
	Input,
	Button,
	Table,
	Label,
	FormGroup,
	Form,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from 'reactstrap';
import './home.css';
import head from '../../img/head.jpg';
import { tsImportEqualsDeclaration, thisExpression } from '@babel/types';
import { get, post, sever } from '../../service/service';
import XlsExport from 'xlsexport';
import { FaLock, FaUserShield, FaPrint } from 'react-icons/fa';
import Swal from 'sweetalert2';
import moment from 'moment';

let api = sever + '/image/room/';

export default class HomeTwo extends Component {
	constructor(props) {
		super(props);

		this.state = {
			room_meeting: [],
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}
	async componentWillMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (!userData) {
			this.props.history.push('/');
		}
	}
	async componentDidMount() {
		let res = await get('/room_meeting');
		if (res.success) {
			this.setState({ room_meeting: res.result.sort((a, b) => b.id_meeting - a.id_meeting) });
		}
	}
	searchText = async (e) => {
		let res = await get('/room_meeting');
		let texts = e.toLowerCase();
		let a = res.result
			.sort((a, b) => b.id_meeting - a.id_meeting)
			.filter(
				(el) =>
					String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.chair_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.detail_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.amount_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.tool_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.location_meeting).toLowerCase().indexOf(texts) > -1 ||
					String(el.deposit).toLowerCase().indexOf(texts) > -1
			);
		this.setState({ room_meeting: a });
	};
	exportXsl = (e) => {
		let { room_meeting } = this.state;
		let download_xls = room_meeting.filter((el) => Number(el.id_meeting) === Number(e)).map((e) => ({
			ไอดี: e.id_meeting,
			ชื่อห้อง: e.name_meeting,
			ประเภทห้อง: e.type_meeting,
			รายระเอียด: e.detail_meeting,
			จำนวนที่นั้ง: e.chair_meeting,
			'สถานที่/ที่ตั้ง': e.location_meeting,
			อุปกรณ์เสริม: e.tool_meeting,
			ราคา: e.amount_meeting,
			ราคามัดจำ: e.deposit
		}));
		// console.log('download_xls', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายละเอียดห้องประชุม');
			xls.exportToXLS(
				'รายละเอียดห้องประชุม ' +
					download_xls[0].ชื่อห้อง +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	exportXslall = () => {
		let { room_meeting } = this.state;
		let download_xls = room_meeting.map((e) => ({
			ไอดี: e.id_meeting,
			ชื่อห้อง: e.name_meeting,
			ประเภทห้อง: e.type_meeting,
			รายระเอียด: e.detail_meeting,
			จำนวนที่นั้ง: e.chair_meeting,
			'สถานที่/ที่ตั้ง': e.location_meeting,
			อุปกรณ์เสริม: e.tool_meeting,
			ราคา: e.amount_meeting
		}));
		// console.log('download_xls', download_xls);
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายละเอียดห้องประชุมทั้งหมด');
			xls.exportToXLS(
				'รายละเอียดห้องประชุมทั้งหมด' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	toggle(e) {
		if (e) {
			this.setState({
				id_meeting: e
			});
		}
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	insert_evaluation = async () => {
		let userData = JSON.parse(localStorage.getItem('data'));
		let { level, report, id_meeting } = this.state;
		let obj = {
			level,
			report,
			id_u: Number(userData.id),
			id_m: Number(id_meeting)
		};
		if (!level) {
			Swal.fire('คำเตือน', 'กรุณาให้คะแนน', 'warning');
		} else {
			try {
				let res = await post('/insert_evaluation', obj);
				if (res.success) {
					Swal.fire('สำเร็จ', 'ประเมินเสร็จสิ้น', 'success').then(() => window.location.reload());
				} else {
					Swal.fire('ผิดพลาด', 'ประเมินไม่สำเร็จ', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
			}
		}
	};
	render() {
		let { room_meeting, modal } = this.state;
		return (
			<div>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>รายระเอียดห้องประชุม</div>
									<div style={{ display: 'flex' }}>
										<Input
											type="search"
											placeholder="ค้นหา"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
										<Button onClick={this.exportXslall} color="secondary">
											<FaPrint />
										</Button>
									</div>
								</div>
							</div>
						</Col>
						<Col xs={12}>
							<Table striped>
								<tbody>
									{room_meeting.length > 0 ? (
										room_meeting.map((e) => (
											<tr>
												<td>
													<div>
														<div style={{ display: 'flex', alignItems: 'center' }}>
															<img
																src={api + e.id_meeting + '.png'}
																width={150}
																height={150}
															/>
															<Container style={{ paddingLeft: '3rem' }}>
																<Row>
																	<Col sm={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>
																					<strong>ชื่อห้อง</strong>
																				</Col>
																				<Col sm={9}>
																					<strong>{e.name_meeting}</strong>
																				</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																	<Col sm={5}>
																		<Form>
																			<FormGroup row>
																				<Col sm={4}>ประเภทห้อง</Col>
																				<Col sm={8}>{e.type_meeting}</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																	<Col sm={1}>
																		<Button
																			size="sm"
																			onClick={() => this.exportXsl(e.id_meeting)}
																			color="primary"
																		>
																			<FaPrint />
																		</Button>
																	</Col>
																	<Col sm={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>สถานที่/ที่ตั้ง</Col>
																				<Col sm={9}>{e.location_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>จำนวนที่นั้ง</Col>
																				<Col sm={9}>
																					{e.chair_meeting.toLocaleString() +
																						' ที่นั่ง'}
																				</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>รายระเอียด</Col>
																				<Col sm={9}>{e.detail_meeting}</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																	<Col sm={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>อุปกรณ์เสริม</Col>
																				<Col sm={9}>{e.tool_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>ราคามัดจำ</Col>
																				<Col sm={9}>
																					{e.deposit.toLocaleString() +
																						' บาท'}
																				</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>ราคา</Col>
																				<Col sm={9}>
																					{e.amount_meeting.toLocaleString() +
																						' บาท'}
																				</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																</Row>
															</Container>
														</div>
														<div
															style={{ display: 'flex', justifyContent: 'space-evenly' }}
														>
															<Button
																color="primary"
																onClick={() =>
																	this.props.history.push({
																		pathname: '/reservations',
																		state: {
																			name_meeting: e.name_meeting,
																			id_meeting: e.id_meeting,
																			location: e.location_meeting,
																			chair_meeting: e.chair_meeting
																		}
																	})}
															>
																จองห้องประชุม
															</Button>
															<Button
																color="info"
																onClick={() => this.toggle(e.id_meeting)}
															>
																ประเมินความพึงพอใจ
															</Button>
														</div>
													</div>
												</td>
											</tr>
										))
									) : (
										<div>---- ไม่มีรายการห้องประชุม -----</div>
									)}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Container>
				<Modal isOpen={modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						ประเมินความพึงพอใจ
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col sm={12}>
									<div
										style={{
											display: 'flex',
											justifyContent: 'space-evenly'
										}}
									>
										<div>
											<label>
												<Input
													type="radio"
													name="level"
													value="ควรปรับปรุง"
													onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
												/>
												ควรปรับปรุง
											</label>
										</div>
										<div>
											<label>
												<Input
													type="radio"
													name="level"
													value="พอใช้"
													onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
												/>
												พอใช้
											</label>
										</div>
										<div>
											<label>
												<Input
													type="radio"
													name="level"
													value="ปานกลาง"
													onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
												/>
												ปานกลาง
											</label>
										</div>
										<div>
											<label>
												<Input
													type="radio"
													name="level"
													value="ดี"
													onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
												/>
												ดี
											</label>
										</div>
										<div>
											<label>
												<Input
													type="radio"
													name="level"
													value="ดีมาก"
													onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
												/>
												ดีมาก
											</label>
										</div>
									</div>
								</Col>
								<Col sm={12}>
									<Input
										placeholder="ข้อเสนอแนะ"
										name="report"
										type="textarea"
										onChange={(e) => this.setState({ [e.target.name]: e.target.value })}
									/>
								</Col>
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={() => this.insert_evaluation()} color="primary">
							ส่ง
						</Button>
						<Button onClick={this.toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
