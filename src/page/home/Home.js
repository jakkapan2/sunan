import React, { Component } from 'react';
import { Container, Row, Col, Input, Button, Table, Label, FormGroup, Form } from 'reactstrap';
import './home.css';
import head from '../../img/head.jpg';
import { tsImportEqualsDeclaration } from '@babel/types';
import { get, post, sever } from '../../service/service';
import Navbar_no_login from '../../compoment/navbar_no_login';
import ModalLogin from '../../compoment/ModalLogin';
import Swal from 'sweetalert2';

let api = sever + '/image/room/';

export default class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			room_meeting: [],
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}

	async componentDidMount() {
		let res = await get('/room_meeting');
		if (res.success) {
			this.setState({ room_meeting: res.result });
		}
	}
	searchText = async (e) => {
		let res = await get('/room_meeting');
		let texts = e.toLowerCase();
		let a = res.result.filter(
			(el) =>
				String(el.name_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.chair_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.type_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.detail_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.amount_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.tool_meeting).toLowerCase().indexOf(texts) > -1 ||
				String(el.location_meeting).toLowerCase().indexOf(texts) > -1
		);
		this.setState({ room_meeting: a });
	};
	login = async (e) => {
		// console.log(e);
		if (!e.password || !e.username) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			try {
				let res = await post('/login', { username: e.username, password: e.password });
				if (res.success) {
					localStorage.setItem('data', JSON.stringify(res.result[0]));
					if (res.result[0].role === 'renter') {
						this.props.history.push('/calendar');
					} else if (res.result[0].role === 'lessor') {
						this.props.history.push('/SaleHomeTwo');
					} else {
						this.props.history.push('/AdminRate');
					}
				} else {
					Swal.fire('ผิดพลาด', res.message, 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
				console.log(error);
			}
		}
	};
	nextGo(name, id) {
		let userData = JSON.parse(localStorage.getItem('data'));
		if (userData) {
			this.props.history.push({
				pathname: '/reservations',
				state: {
					name_meeting: name,
					id_meeting: id
				}
			});
		} else {
			this.toggle();
		}
	}
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	regis = async (e) => {
		let { role, fname, lname, phone, token_line, username, password, password2 } = e;
		if (!role || !fname || !lname || !phone || !token_line || !username || !password || !password2) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			if (password !== password2) {
				Swal.fire('คำเตือน', 'password ไม่ตรงกัน', 'warning');
			} else {
				let user = await get('/user_all');
				let check = user.result.some(
					(e) => String(e.username).toLowerCase() === String(username).toLowerCase()
				);
				if (check) {
					Swal.fire('คำเตือน', 'username นี้ถูกใช้ไปแล้ว', 'warning');
				} else {
					try {
						let res = await post('/insert_user', e);
						if (res.success) {
							Swal.fire('สำเร็จ', 'สมัครสมาชิกเสร็จสิ้น', 'success').then(async () => {
								let ress = await post('/login', { username, password });
								localStorage.setItem('data', JSON.stringify(ress.result[0]));
								if (ress.result[0].role === 'renter') {
									this.props.history.push('/calendar');
								} else if (ress.result[0].role === 'lessor') {
									this.props.history.push('/SaleHomeTwo');
								} else {
									this.props.history.push('/AdminRate');
								}
							});
						} else {
							Swal.fire('ผิดพลาด', 'ไม่สามารถสมัครสมาชิกได้', 'error');
						}
					} catch (error) {
						Swal.fire('ผิดพลาด', error, 'error');
					}
				}
			}
		}
	};
	loginEnter = async (e, k) => {
		if (k.key === 'Enter') {
			if (!e.password || !e.username) {
				alert('กรอกข้อมูลไม่ครบ');
			} else {
				try {
					let res = await post('/login', { username: e.username, password: e.password });
					if (res.success) {
						localStorage.setItem('data', JSON.stringify(res.result[0]));
						if (res.result[0].role === 'renter') {
							this.props.history.push('/calendar');
						} else if (res.result[0].role === 'lessor') {
							this.props.history.push('/SaleHomeTwo');
						} else {
							this.props.history.push('/AdminRate');
						}
					} else {
						alert(res.message);
					}
				} catch (error) {
					alert('ผิดพลาด', error);
					console.log(error);
				}
			}
		}
	};
	render() {
		let { room_meeting, modal } = this.state;
		return (
			<div>
				<ModalLogin
					authen={true}
					modal={modal}
					toggle={this.toggle}
					login={(e) => this.login(e)}
					enter={(obj, key) => this.loginEnter(obj, key)}
				/>
				<Navbar_no_login
					login={(e) => this.login(e)}
					regis={(e) => this.regis(e)}
					enter={(obj, key) => this.loginEnter(obj, key)}
				/>
				<Container style={{ paddingTop: '1rem' }}>
					<Row>
						<Col xs={12}>
							<div>
								<div id="cssheader">
									<div style={{ paddingRight: '10px', color: '#fff' }}>รายระเอียดห้องประชุม</div>
									<div>
										<Input
											type="search"
											placeholder="ค้นหารายการ"
											onChange={(e) => this.searchText(e.target.value)}
											// style={{ float: "right" }}
										/>
									</div>
								</div>
							</div>
						</Col>
						<Col xs={12}>
							<Table striped>
								<tbody>
									{room_meeting.length > 0 ? (
										room_meeting.map((e) => (
											<tr>
												<td>
													<div>
														<div style={{ display: 'flex', alignItems: 'center' }}>
															<img
																src={api + e.id_meeting + '.png'}
																width={150}
																height={150}
															/>
															<Container style={{ paddingLeft: '3rem' }}>
																<Row>
																	<Col xs={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>
																					<strong>ชื่อห้อง</strong>
																				</Col>
																				<Col sm={9}>
																					<strong>{e.name_meeting}</strong>
																				</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																	<Col xs={6} />
																	<Col sm={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>ประเภทห้อง</Col>
																				<Col sm={9}>{e.type_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>รายระเอียด</Col>
																				<Col sm={9}>{e.detail_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>จำนวนที่นั้ง</Col>
																				<Col sm={9}>
																					{e.chair_meeting.toLocaleString() +
																						' ที่นั่ง'}
																				</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																	<Col sm={6}>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>สถานที่/ที่ตั้ง</Col>
																				<Col sm={9}>{e.location_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>อุปกรณ์เสริม</Col>
																				<Col sm={9}>{e.tool_meeting}</Col>
																			</FormGroup>
																		</Form>
																		<Form>
																			<FormGroup row>
																				<Col sm={3}>ราคา</Col>
																				<Col sm={9}>
																					{e.amount_meeting.toLocaleString() +
																						' บาท'}
																				</Col>
																			</FormGroup>
																		</Form>
																	</Col>
																</Row>
															</Container>
														</div>
														<div style={{ display: 'flex', justifyContent: 'center' }}>
															<Button
																color="primary"
																onClick={() =>
																	this.nextGo(e.name_meeting, e.id_meeting)}
															>
																จองห้องประชุม
															</Button>
														</div>
													</div>
												</td>
											</tr>
										))
									) : (
										<div>---- ไม่มีรายการห้องประชุม -----</div>
									)}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}
