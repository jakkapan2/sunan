import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import App from './App';
import Home from '../page/home/Home';
import UseRoomCalendar from '../page/member/UseRoomCalendar';
import navbar_no_login from '../compoment/navbar_no_login';
const data = JSON.parse(localStorage.getItem('data'));
export default () => (
	<BrowserRouter>
		<Switch>
			{/* <Route exact path="/" component={navbar_no_login} /> */}
			<Route exact path="/" component={UseRoomCalendar} />
			<Route exact path="/listmeetingroom" component={Home} />
			<App />
		</Switch>
	</BrowserRouter>
);
