import React from 'react';
import { Route } from 'react-router-dom';
import Start from '../page/Start';
import Reservations from '../page/member/Reservations';
import ReservationsTwo from '../page/member/Reservations-Two';
import UseRoomToday from '../page/member/UseRoomToday';
import CheckRoomReservations from '../page/member/CheckRoomReservations';
import CheckRoomavailability from '../page/member/CheckRoomavailability';
import Addroom from '../page/sale/Addroom';
import Editprofile from '../page/member/Editprofile';
import CheckRoom from '../page/member/CheckRoom';
import History from '../page/member/History';
import HomeTwo from '../page/home/HomeTwo';
import UseRoomCalendarTwo from '../page/member/UseRoomCalendarTwo';
// import Admin from "../page/admin/Adminlog";
// ผู้ให้เช่า
import SaleHomeTwo from '../page/sale/SaleHomeTwo';
import editroom from '../page/sale/Editroom';
import SaleUseRoomToday from '../page/sale/SaleUseRoomToday';
import SaleCheckRoomReservations from '../page/sale/SaleCheckRoomReservations';
import Rate from '../page/sale/Rate';
import SaleCheckRoomavailability from '../page/sale/SaleCheckRoomavailability';
// Admin
import AdminRate from '../page/admin/AdminRate';
import AdminCheckRoom from '../page/admin/AdminCheckRoom';
export default () => (
	<Start>
		{/**----------------------------------------------------------------------------------------- ผู้เช่า */}
		{/**-----  Home ------ */}
		<Route exact path="/calendar" component={UseRoomCalendarTwo} />
		{/**-----  จองห้องประชุม ------ */}
		<Route exact path="/listlogin" component={HomeTwo} />
		{/**---- รายละเอียดที่จะจอง ---- */}
		<Route exact path="/reservations" component={Reservations} />
		{/**---- แก้ไขรายละเอียดที่จะจอง ----- */}
		<Route exact path="/reservationstwo" component={ReservationsTwo} />
		{/**------------- รายการจองของฉัน ------------------------ */}
		<Route exact path="/userroomtoday" component={UseRoomToday} />
		{/**----- การชำระเงิน  ------ */}
		<Route exact path="/checkroomreservations" component={CheckRoomReservations} />
		{/**----- ประวัติการชำระเงิน  ------ */}
		<Route exact path="/checkRoomavailability" component={CheckRoomavailability} />

		<Route exact path="/editprofile" component={Editprofile} />
		<Route exact path="/history" component={History} />
		{/**---------------------------------------------------------------------------------------- ผู้ให้เช่า */}
		{/**-----  ห้องประชุม ------ */}
		<Route exact path="/SaleHomeTwo" component={SaleHomeTwo} />
		{/**------------- เพิ่มห้องประชุม ------------------------ */}
		<Route exact path="/addroom" component={Addroom} />
		{/**------------- แก้ไขห้องประชุม ------------------------ */}
		<Route exact path="/editroom" component={editroom} />
		{/**------------- การจอง ------------------------ */}
		<Route exact path="/SaleUseRoomToday" component={SaleUseRoomToday} />
		{/**----- การชำระเงิน  ------ */}
		<Route exact path="/SaleCheckRoomReservations" component={SaleCheckRoomReservations} />
		{/**----- ผลการประเมินความพึงพอใจ  ------ */}
		<Route exact path="/Rate" component={Rate} />
		{/**----- การชำระเงินการลงทะเบียน  ------ */}
		<Route exact path="/CheckRoom" component={CheckRoom} />
		{/**----- ประวัติการชำระเงิน  ------ */}
		<Route exact path="/SaleCheckRoomavailability" component={SaleCheckRoomavailability} />
		{/**---------------------------------------------------------------------------------------- Admin */}
		{/**-----  ผลการประเมินความพึงพอใจ ------ */}
		<Route exact path="/AdminRate" component={AdminRate} />
		{/**----- สถานะการชำระค่าลงทะเบียน  ------ */}
		<Route exact path="/AdminCheckRoom" component={AdminCheckRoom} />
	</Start>
);
