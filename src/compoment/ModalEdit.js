import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Button,
	Container,
	Row,
	Col,
	Input,
	Label,
	Form,
	FormGroup
} from 'reactstrap';
import Swal from 'sweetalert2';
import { get, post } from '../service/service';

export default class ModalEdit extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modal: false,
			role: 'renter'
		};
		this.toggle = this.toggle.bind(this);
	}
	async componentDidMount() {
		let userData = JSON.parse(localStorage.getItem('data'));
		try {
			let res = await post('/getuser', { id: Number(userData.id) });
			this.setState({
				fname: res.result[0].fname,
				lname: res.result[0].lname,
				token_line: res.result[0].token_line,
				phone: res.result[0].phone,
				username: res.result[0].username,
				password: res.result[0].password,
				PromptPay: res.result[0].PromptPay,
				Bank: res.result[0].Bank,
				userRole: userData.role
			});
		} catch (error) {
			console.log(error);
		}
	}

	onChangeState = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	register = async () => {
		let userData = JSON.parse(localStorage.getItem('data'));
		let { fname, lname, phone, token_line, username, password, PromptPay, Bank } = this.state;

		if (!fname || !lname || !phone || !token_line || !username || !password) {
			Swal.fire('คำเตือน', 'กรอกข้อมูลไม่ครบ', 'warning');
		} else {
			// if (password !== password2) {
			// 	Swal.fire('คำเตือน', 'password ไม่ตรงกัน', 'warning');
			// } else {
			// let user = await get('/user_all');
			// let check = user.result.some((e) => e.username === username);
			// if (check) {
			// 	Swal.fire('คำเตือน', 'username นี้ถูกใช้ไปแล้ว', 'warning');
			// } else {
			let obj = { fname, lname, phone, token_line, id: Number(userData.id), username, password, PromptPay, Bank };
			try {
				let res = await post('/updateData', obj);
				if (res.success) {
					Swal.fire('สำเร็จ', 'แก้ไขข้อมูลส่วนตัวเสร็จสิ้น', 'success').then(() => window.location.reload());
				} else {
					Swal.fire('ผิดพลาด', 'ไม่สามารถแก้ไขข้อมูลส่วนตัวได้', 'error');
				}
			} catch (error) {
				Swal.fire('ผิดพลาด', error, 'error');
			}
			// }
			// }
		}
	};
	render() {
		let { role, fname, lname, token_line, phone, username, password, userRole, PromptPay, Bank } = this.state;
		let { modal, toggle } = this.props;
		return (
			<div>
				<Modal isOpen={modal} toggle={toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={toggle}>
						แก้ไขข้อมูลส่วนตัว
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<div>
										<Form>
											<FormGroup row>
												<Label sm={4}>ชื่อ</Label>
												<Col sm={8}>
													<Input
														placeholder="ชื่อ"
														name="fname"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
														value={fname}
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>นามสกุล</Label>
												<Col sm={8}>
													<Input
														placeholder="นามสกุล"
														name="lname"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
														value={lname}
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>Line Token</Label>
												<Col sm={8}>
													<Input
														placeholder="Line Token"
														name="token_line"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
														value={token_line}
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>เบอร์โทร</Label>
												<Col sm={8}>
													<Input
														placeholder="เบอร์โทร"
														name="phone"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="number"
														value={phone}
													/>
												</Col>
											</FormGroup>
										</Form>
										{(userRole === 'lessor' || userRole === 'admin') && (
											<div>
												<hr />
												<Form>
													<FormGroup row>
														<Label sm={4}>พร้อมเพย์</Label>
														<Col sm={8}>
															<Input
																placeholder="หมายเลขพร้อมเพย์"
																name="PromptPay"
																onChange={(e) => this.onChangeState(e)}
																style={{ float: 'right' }}
																type="text"
																value={PromptPay}
															/>
														</Col>
													</FormGroup>
												</Form>
												<Form>
													<FormGroup row>
														<Label sm={4}>บัญชีธนาคาร</Label>
														<Col sm={8}>
															<Input
																placeholder="บัญชีธนาคาร หมายเลขบัญชี"
																name="Bank"
																onChange={(e) => this.onChangeState(e)}
																style={{ float: 'right' }}
																type="text"
																value={Bank}
															/>
														</Col>
													</FormGroup>
												</Form>
											</div>
										)}

										<hr />
										<Form>
											<FormGroup row>
												<Label sm={4}>ชื่อผู้ใช้งาน</Label>
												<Col sm={8}>
													<Input
														placeholder="ชื่อผู้ใช้งาน"
														name="username"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
														value={username}
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>รหัสผ่าน</Label>
												<Col sm={8}>
													<Input
														placeholder="รหัสผ่าน"
														name="password"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="password"
														value={password}
													/>
												</Col>
											</FormGroup>
										</Form>
									</div>
								</Col>
								<Col xs={12} />
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={() => this.register()} color="primary">
							ตกลง
						</Button>
						<Button onClick={toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
