import React from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Row,
	Col,
	Input
} from 'reactstrap';
import '../page/navbar.css';
import ModalRegister from './ModalRegister';
import ModalLogin from './ModalLogin';

export default class NavbarNoLogin extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isOpen: false
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	render() {
		let { login, regis, enter } = this.props;
		return (
			<div>
				<Navbar id="back-img" expand="md">
					<NavbarBrand href="/">
						<text id="hover-text">Meeting Room System Online</text>
					</NavbarBrand>
					<NavbarToggler />
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className="ml-auto" navbar>
							<UncontrolledDropdown nav inNavbar>
								<DropdownToggle nav caret>
									<text id="hover-text">เพิ่มเติม</text>
								</DropdownToggle>
								<DropdownMenu right>
									<DropdownItem href="/">Home</DropdownItem>
									<DropdownItem href="/listmeetingroom">ห้องประชุม</DropdownItem>
									<DropdownItem divider />
									<DropdownItem>
										<ModalRegister regis={(e) => regis(e)} />
									</DropdownItem>
									<DropdownItem>
										<ModalLogin
											authen={false}
											login={(e) => login(e)}
											enter={(obj, key) => enter(obj, key)}
										/>
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
						</Nav>
					</Collapse>
				</Navbar>
			</div>
		);
	}
}
