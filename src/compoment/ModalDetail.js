import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Container, Row, Col, Input, Form, FormGroup, Label, Button } from 'reactstrap';
import moment from 'moment';
import XlsExport from 'xlsexport';
import { FaLock, FaUserShield, FaPrint } from 'react-icons/fa';

export default class ModalDetail extends Component {
	componentDidMount() {
		let th = require('moment/locale/th');
		moment.updateLocale('th', th);
	}
	exportXsl = () => {
		let { modalDetail } = this.props;
		let booking = [ modalDetail ];
		let download_xls = booking.map((e) => ({
			ไอดี: e.id_booking,
			'สถานที่/ที่ตั้ง': e.location_meeting,
			จำนวนที่นั่ง: e.chair_meeting,
			ชื่อผู้จอง: e.fname + ' ' + e.lname,
			โทรศัพท์: e.phone,
			วันที่:
				moment(e.datestart_booking).add(543, 'y').format('LL') +
				' เวลา ' +
				e.timestart_booking +
				' นน.' +
				' ถึง ' +
				moment(e.dateend_booking).add(543, 'y').format('LL') +
				' เวลา ' +
				e.timeend_booking +
				' นน.',
			อุปกรณ์เสริม: !e.tool_name ? '-' : e.tool_name,
			สถานะ: e.status_booking
		}));
		if (download_xls.length > 0) {
			const xls = new XlsExport(download_xls, 'รายการจอง');
			xls.exportToXLS(
				'รายการจองที่' +
					' ' +
					modalDetail.id_booking +
					' ' +
					'(' +
					moment(new Date()).format('DD MMMM') +
					' ' +
					(Number(moment(new Date()).format('YYYY')) + 543) +
					')' +
					'.xls'
			);
		}
	};
	render() {
		let { modal, close, modalDetail, check } = this.props;
		return (
			<Modal isOpen={modal} toggle={close}>
				<ModalHeader id="style-modal-head" toggle={close}>
					รายละเอียดของ การจอง
				</ModalHeader>
				<ModalBody>
					<Container style={{ paddingTop: '1rem' }}>
						<Row>
							<Col xs={12}>
								<Form>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											สถานที่/ที่ตั้ง
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{modalDetail.location_meeting}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											จำนวนที่นั่ง
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{modalDetail.chair_meeting && modalDetail.chair_meeting.toLocaleString()}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											ชื่อผู้จอง
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{modalDetail.fname + ' ' + modalDetail.lname}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											โทรศัพท์
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{modalDetail.phone}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											วันที่
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{moment(modalDetail.datestart_booking).add(543, 'y').format('LL') +
												' เวลา ' +
												modalDetail.timestart_booking +
												' นน.' +
												' ถึง ' +
												moment(modalDetail.dateend_booking).add(543, 'y').format('LL') +
												' เวลา ' +
												modalDetail.timeend_booking +
												' นน.'}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											อุปกรณ์เสริม
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											{!modalDetail.tool_name ? '-' : modalDetail.tool_name}
										</Col>
									</FormGroup>
									<FormGroup row>
										<Col sm={3} style={{ border: '1px solid #c8c8c8' }}>
											สถานะ
										</Col>
										<Col sm={9} style={{ border: '1px solid #c8c8c8' }}>
											<span
												style={{
													background:
														modalDetail.status_booking === 'รอตรวจสอบ'
															? '#008ae6'
															: modalDetail.status_booking === 'รอการชำระเงิน'
																? '#4dd2ff'
																: '#4ce600',
													color: '#fff',
													padding: '0.1rem 0.3rem 0.1rem 0.3rem'
												}}
											>
												{modalDetail.status_booking}
											</span>
										</Col>
									</FormGroup>
									{check && (
										<div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
											<Button size="sm" onClick={this.exportXsl} color="primary">
												<FaPrint />
											</Button>
										</div>
									)}
								</Form>
							</Col>
						</Row>
					</Container>
				</ModalBody>
			</Modal>
		);
	}
}
