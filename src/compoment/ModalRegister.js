import React, { Component } from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Button,
	Container,
	Row,
	Col,
	Input,
	Label,
	Form,
	FormGroup
} from 'reactstrap';
import Swal from 'sweetalert2';
import { get, post } from '../service/service';

export default class ModalRegister extends Component {
	constructor(props) {
		super(props);

		this.state = {
			modal: false,
			role: 'renter'
		};
		this.toggle = this.toggle.bind(this);
	}
	onChangeState = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};
	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal
		}));
	}
	register = async () => {
		let { role, fname, lname, phone, token_line, username, password, password2 } = this.state;
		let obj = { role, fname, lname, phone, token_line, username, password, password2 };
		this.props.regis(obj);
	};
	render() {
		let { role } = this.state;
		return (
			<div>
				<text onClick={this.toggle}>สมัครสมาชิก</text>
				<Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
					<ModalHeader id="style-modal-head" toggle={this.toggle}>
						สมัครสมาชิก
					</ModalHeader>
					<ModalBody>
						<Container style={{ paddingTop: '1rem' }}>
							<Row>
								<Col xs={12}>
									<div>
										<Form>
											<FormGroup row>
												<Label sm={4}>ประเภท</Label>
												<Col sm={8}>
													<div
														style={{
															display: 'flex',
															justifyContent: 'space-evenly'
														}}
													>
														<div>
															<label>
																<Input
																	checked={role === 'renter'}
																	type="radio"
																	name="role"
																	value="renter"
																	onChange={(e) => this.onChangeState(e)}
																/>
																ผู้เช่า
															</label>
														</div>
														<div>
															<label>
																<Input
																	checked={role === 'lessor'}
																	type="radio"
																	name="role"
																	value="lessor"
																	onChange={(e) => this.onChangeState(e)}
																/>
																ผู้ให้เช่า
															</label>
														</div>
													</div>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>ชื่อ</Label>
												<Col sm={8}>
													<Input
														placeholder="ชื่อ"
														name="fname"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>นามสกุล</Label>
												<Col sm={8}>
													<Input
														placeholder="นามสกุล"
														name="lname"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>Line Token</Label>
												<Col sm={8}>
													<Input
														placeholder="Line Token"
														name="token_line"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>เบอร์โทร</Label>
												<Col sm={8}>
													<Input
														placeholder="เบอร์โทร"
														name="phone"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="number"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>ชื่อผู้ใช้งาน</Label>
												<Col sm={8}>
													<Input
														placeholder="ชื่อผู้ใช้งาน"
														name="username"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="text"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>รหัสผ่าน</Label>
												<Col sm={8}>
													<Input
														placeholder="รหัสผ่าน"
														name="password"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="password"
													/>
												</Col>
											</FormGroup>
										</Form>
										<Form>
											<FormGroup row>
												<Label sm={4}>ยืนยันรหัสผ่าน</Label>
												<Col sm={8}>
													<Input
														placeholder="ยืนยันรหัสผ่าน"
														name="password2"
														onChange={(e) => this.onChangeState(e)}
														style={{ float: 'right' }}
														type="password"
													/>
												</Col>
											</FormGroup>
										</Form>
									</div>
								</Col>
								<Col xs={12} />
							</Row>
						</Container>
					</ModalBody>
					<ModalFooter>
						<Button onClick={() => this.register()} color="primary">
							ตกลง
						</Button>
						<Button onClick={this.toggle} color="danger">
							ยกเลิก
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
